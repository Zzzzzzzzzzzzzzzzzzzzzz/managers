﻿using System;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using System.Collections.Generic;

namespace BuffManager
{
    public class BuffManager : AOPluginEntry
    {
        protected Settings _settings;

        private static double _timer;
        private static bool Toggle = false;

        private static string _removeName = string.Empty;

        public static Window _infoWindow;

        public static View _infoView;

        public static List<string> _targets = new List<string>();
        public static List<string> _buffNames = new List<string>();

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("BuffManager");
            PluginDir = pluginDir;

            Game.OnUpdate += OnUpdate;

            _settings.AddVariable("Toggle", false);

            Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["Toggle"] = !_settings["Toggle"].AsBool();
                Chat.WriteLine($"Buff : {_settings["Toggle"].AsBool()}");
            });

            if (_settings["Toggle"].AsBool())
                Toggle = true;

            RegisterSettingsWindow("Buff Manager", "BuffManagerSettingsView.xml");

            Chat.WriteLine("BuffManager Loaded! Version: 0.9.9.83");
            Chat.WriteLine("/buffmanager for settings.");
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void HandleAddTargetViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _targets.Add(Targeting.TargetChar?.Name);
        }

        private void HandleRemoveTargetViewClick(object s, ButtonBase button)
        {
            if (Targeting.TargetChar?.Name != DynelManager.LocalPlayer.Name) _targets.Remove(Targeting.TargetChar?.Name);
        }

        private void HandleClearTargetsViewClick(object s, ButtonBase button)
        {
            _targets.Clear();
        }

        private void HandlePrintTargetsViewClick(object s, ButtonBase button)
        {
            foreach (string str in _targets)
                Chat.WriteLine(str);
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\BuffManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F4) && !_init)
            //{
            //    _init = true;

            //    Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\HelpManager\\{Game.ClientInst}\\Config.json");

            //    SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 250, 300), "Help Manager", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

            //    if (SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsVisible)
            //    {
            //        foreach (string settingsName in SettingsController.settingsWindows.Keys.Where(x => x.Contains("Help Manager")))
            //        {
            //            SettingsController.AppendSettingsTab(settingsName, SettingsController.settingsWindow);

            //            SettingsController.settingsWindow.FindView("AssistNamedCharacter", out TextInputView assistInput);

            //            if (assistInput != null)
            //                assistInput.Text = Config.CharSettings[Game.ClientInst].AssistPlayer;
            //        }
            //    }

            //    _init = false;
            //}

            if (Game.IsZoning)
                return;

            if (_settings["Toggle"].AsBool())
            {
                SimpleChar _player = DynelManager.Players
                    .Where(c => _targets.Contains(c.Name)
                        && c.Health > 0)
                    .FirstOrDefault();

                if (_player != null)
                {
                    foreach (Buff _buff in _player.Buffs.Where(c => Spell.List.Select(m => m.Id).Contains(c.Id)))
                    {
                        if (_buff.RemainingTime < 3600f && !Spell.HasPendingCast)
                            Spell.List.FirstOrDefault(c => c.Id == _buff.Id).Cast(_player);
                    }
                }
            }

            #region UI Update

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                if (SettingsController.settingsWindow.FindView("BuffManagerAddTarget", out Button addTargetView))
                {
                    addTargetView.Tag = SettingsController.settingsWindow;
                    addTargetView.Clicked = HandleAddTargetViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffManagerRemoveTarget", out Button removeTargetView))
                {
                    removeTargetView.Tag = SettingsController.settingsWindow;
                    removeTargetView.Clicked = HandleRemoveTargetViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffManagerClearTargets", out Button clearTargetsView))
                {
                    clearTargetsView.Tag = SettingsController.settingsWindow;
                    clearTargetsView.Clicked = HandleClearTargetsViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffManagerPrintTargets", out Button printTargetsView))
                {
                    printTargetsView.Tag = SettingsController.settingsWindow;
                    printTargetsView.Clicked = HandlePrintTargetsViewClick;
                }

                if (SettingsController.settingsWindow.FindView("BuffManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Toggle = true;
                    Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Toggle = false;
                    Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
                }
            }

            #endregion
        }

        public enum ModeSelection
        {
            Relay, Replenish
        }

        public static bool CanRelay(Buff _buff)
        {
            return _buff.Nanoline == NanoLine.AMS || _buff.Id == RelevantBuffs.Survival || _buff.Id == RelevantBuffs.Sphere
                || _buff.Id == RelevantBuffs.WitOfTheAtrox || _buff.Id == RelevantBuffs.Sacrifice || _buff.Id == RelevantBuffs.BioRegrowth
                || RelevantBuffs.BioCocoons.Contains(_buff.Id);
        }

        public static class RelevantBuffs
        {
            public const int Survival = 253046;
            public const int Sphere = 253031;
            public const int WitOfTheAtrox = 252506;
            public const int Sacrifice = 248237;
            public const int BioRegrowth = 215598;
            public static int[] BioCocoons = new[] { 215576, 215575, 215574, 215573, 215572, 215571, 215570, 215569 };
            //public static readonly int[] MatLocBuffs = Spell.GetSpellsForNanoline(NanoLine.MatLocBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
        }
    }
}
