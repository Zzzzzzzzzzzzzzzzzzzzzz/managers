﻿using System;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Core.Inventory;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

namespace BagManager
{
    public class BagManager : AOPluginEntry
    {
        public static Config Config { get; private set; }

        protected Settings _settings;

        private static List<string> _itemsInv = new List<string>();
        private static List<Backpack> _targetBags = new List<Backpack>();
        private static Backpack _targetBag;

        private static string BagName;

        private static double _timer;

        private static bool Toggle = false;
        private static bool _initBags = false;
        private static bool _init = false;

        public static Window _infoWindow;

        public static View _infoView;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("BagManager");
            PluginDir = pluginDir;

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\BagManager\\{Game.ClientInst}\\Config.json");

            Config.CharSettings[Game.ClientInst].BagNameChangedEventChangedEvent += BagName_Changed;

            Game.OnUpdate += OnUpdate;
            Network.N3MessageSent += Network_N3MessageSent;
            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("Toggle", false);
            _settings.AddVariable("OpenBags", false);

            _settings["Toggle"] = false;
            _settings["OpenBags"] = false;

            _settings.AddVariable("ModeSelection", (int)ModeSelection.Swap);

            Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (ModeSelection.Trade == (ModeSelection)_settings["ModeSelection"].AsInt32())
                {
                    foreach (string str in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory).Select(c => c.Name))
                    {
                        if (!_itemsInv.Contains(str))
                            _itemsInv.Add(str);
                    }
                }

                _settings["Toggle"] = !_settings["Toggle"].AsBool();
                Toggle = !Toggle;
                Chat.WriteLine($"Bag : {_settings["Toggle"].AsBool()}");
            });

            if (_settings["Toggle"].AsBool())
                Toggle = true;

            RegisterSettingsWindow("Bag Manager", "BagManagerSettingsView.xml");

            Chat.WriteLine("BagManager Loaded! Version: 0.9.9.90");
            Chat.WriteLine("/bagmanager for settings.");

            BagName = Config.CharSettings[Game.ClientInst].BagName;
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void BagName_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].BagName = e;
            BagName = e;
            Config.Save();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\BagManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void HandleDeleteButtonClick(object s, ButtonBase button)
        {
            if (string.IsNullOrEmpty(BagName)) { return; }

            _targetBags = Inventory.Backpacks.Where(c => c.Name.Contains($"{BagName}") && c.IsOpen && c.Items.Count > 0).ToList();

            if (_targetBags.Count() > 0)
            {
                foreach (Backpack _bag in _targetBags)
                {
                    foreach (Item _item in _bag?.Items)
                    {
                        _item?.Delete();
                    }
                }

                Chat.WriteLine("Deleted.");
            }
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (!_settings["Toggle"].AsBool()) { return; }

            if (ModeSelection.Swap != (ModeSelection)_settings["ModeSelection"].AsInt32()) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.UseItemOnItem)
                {
                    Backpack _source = Inventory.Backpacks.Where(x => x.Slot.Instance == charActionMsg.Parameter2).FirstOrDefault();
                    Backpack _target = Inventory.Backpacks.Where(x => x.Slot == charActionMsg.Target).FirstOrDefault();

                    if (_target.Items.Count >= 1)
                    {
                        foreach (Item _item in _target.Items)
                        {
                            Network.Send(new ClientContainerAddItem()
                            {
                                Target = _source.Identity,
                                Source = _item.Slot
                            });
                        }
                    }
                }
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F4) && !_init)
            //{
            //    _init = true;

            //    Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\HelpManager\\{Game.ClientInst}\\Config.json");

            //    SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 250, 300), "Help Manager", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

            //    if (SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsVisible)
            //    {
            //        foreach (string settingsName in SettingsController.settingsWindows.Keys.Where(x => x.Contains("Help Manager")))
            //        {
            //            SettingsController.AppendSettingsTab(settingsName, SettingsController.settingsWindow);

            //            SettingsController.settingsWindow.FindView("AssistNamedCharacter", out TextInputView assistInput);

            //            if (assistInput != null)
            //                assistInput.Text = Config.CharSettings[Game.ClientInst].AssistPlayer;
            //        }
            //    }

            //    _init = false;
            //}

            if (_settings["OpenBags"].AsBool()
                && !_initBags)
            {
                List<Item> _bags = Inventory.Items
                    .Where(c => c.UniqueIdentity.Type == IdentityType.Container)
                    .ToList();

                foreach (Item _bag in _bags)
                {
                    _bag.Use();
                    _bag.Use();
                }

                _initBags = true;
            }

            if (ModeSelection.Trade == (ModeSelection)_settings["ModeSelection"].AsInt32()
                && _settings["Toggle"].AsBool() && Time.NormalTime > _timer + 1f && Toggle)
            {
                if (!string.IsNullOrEmpty(BagName))
                    _targetBags = Inventory.Backpacks.Where(c => c.Name.Contains($"{BagName}") && c.IsOpen && c.Items.Count > 0).ToList();

                if (_targetBags.Count() > 0)
                {
                    foreach (Backpack _bag in _targetBags)
                    {
                        foreach (Item _item in _bag?.Items.Take(Inventory.NumFreeSlots))
                        {
                            _item.MoveToInventory();
                        }
                    }
                }

                foreach (Item _item in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory && c.UniqueIdentity == Identity.None))
                {
                    if (_itemsInv.Contains(_item.Name)) continue;

                    Network.Send(new TradeMessage()
                    {
                        Unknown1 = 2,
                        Action = (TradeAction)5,
                        Param1 = (int)DynelManager.LocalPlayer.Identity.Type,
                        Param2 = DynelManager.LocalPlayer.Identity.Instance,
                        Param3 = (int)IdentityType.Inventory,
                        Param4 = _item.Slot.Instance
                    });
                }

                _timer = Time.NormalTime;
            }

            if (ModeSelection.Sort == (ModeSelection)_settings["ModeSelection"].AsInt32()
                && _settings["Toggle"].AsBool() && Toggle)
            {
                if (string.IsNullOrEmpty(BagName)) { return; }

                _targetBags = Inventory.Backpacks.Where(c => c.Name.Contains($"{BagName}") && c.IsOpen && c.Items.Count() > 0).ToList();

                if (_targetBags.Count() > 0)
                {
                    foreach (Backpack _bag in _targetBags)
                    {
                        foreach (Item _item in _bag.Items)
                        {
                            if (Inventory.Backpacks.Where(c => c.Items.Count() < 21).Any(c => _item.Name.Split().Contains(c.Name)))
                                _targetBag = Inventory.Backpacks.Where(c => c.Items.Count() < 21).FirstOrDefault(c => _item.Name.Split().Contains(c.Name));

                            if (_targetBag != null)
                                _item.MoveToContainer(_targetBag);
                        }
                    }
                }
            }

            #region UI Update

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("BagName", out TextInputView tradeBagNameInput);

                if (tradeBagNameInput != null && !string.IsNullOrEmpty(tradeBagNameInput.Text))
                    if (Config.CharSettings[Game.ClientInst].BagName != tradeBagNameInput.Text)
                        Config.CharSettings[Game.ClientInst].BagName = tradeBagNameInput.Text;

                if (SettingsController.settingsWindow.FindView("BagManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("DeleteButton", out Button deleteButton))
                {
                    deleteButton.Tag = SettingsController.settingsWindow;
                    deleteButton.Clicked = HandleDeleteButtonClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (ModeSelection.Trade == (ModeSelection)_settings["ModeSelection"].AsInt32())
                    {
                        foreach (string str in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory).Select(c => c.Name))
                        {
                            if (!_itemsInv.Contains(str))
                                _itemsInv.Add(str);
                        }
                    }

                    Toggle = true;
                    Chat.WriteLine($"Bag : {_settings["Toggle"].AsBool()}");
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Toggle = false;
                    Chat.WriteLine($"Bag : {_settings["Toggle"].AsBool()}");
                }
            }

            #endregion
        }
        private void OnZoned(object s, EventArgs e)
        {
            if (_settings["OpenBags"].AsBool())
            {
                List<Item> _initBagsItem = Inventory.Items
                    .Where(c => c.UniqueIdentity.Type == IdentityType.Container)
                    .ToList();

                foreach (Item _initBag in _initBagsItem)
                {
                    _initBag.Use();
                    _initBag.Use();
                }
            }
        }

        public static bool IsBackpack(Item item)
        {
            return item.Id == 275381 || item.Id == 143832 || item.Id == 157684 || item.Id == 157689 || item.Id == 157686 ||
                item.Id == 157691 || item.Id == 157692 || item.Id == 157693 || item.Id == 157683 || item.Id == 157682 ||
                item.Id == 287434 || item.Id == 157687 || item.Id == 157688 || item.Id == 157694 || item.Id == 157695 ||
                item.Id == 157690 || item.Id == 99241 || item.Id == 304586 || item.Id == 158790 || item.Id == 99228 ||
                item.Id == 223770 || item.Id == 152039 || item.Id == 156831 || item.Id == 259016 || item.Id == 259382 ||
                item.Id == 287419 || item.Id == 287420 || item.Id == 287421 ||
                item.Id == 287422 || item.Id == 287423 || item.Id == 287424 || item.Id == 287425 ||
                item.Id == 287427 || item.Id == 287429 || item.Id == 287430 ||
                item.Id == 287432 || item.Id == 287434 ||
                item.Id == 287440 || item.Id == 287441 ||
                item.Id == 287442 || item.Id == 287443 || item.Id == 287444 || item.Id == 287445 || item.Id == 287446 ||
                item.Id == 287447 || item.Id == 287448 || item.Id == 287610 ||
                item.Id == 287615 ||
                item.Id == 287617 || item.Id == 287619 || item.Id == 287620 || item.Id == 296977;
        }

        public enum ModeSelection
        {
            Swap, Sort, Trade
        }
    }
}
