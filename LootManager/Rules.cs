﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LootManager
{
    public class Rule
    {
        public string Name = "";
        public string MinQl = "";
        public string MaxQl = "";

        public Rule(string Name, string MinQl, string MaxQl)
        {
            this.Name = Name;
            this.MinQl = MinQl;
            this.MaxQl = MaxQl;
        }

    }
}
