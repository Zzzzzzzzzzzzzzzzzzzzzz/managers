﻿using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LootManager.IPCMessages
{
    [AoContract((int)IPCOpcode.AddItem)]
    public class AddItemMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.AddItem;

        [AoMember(0, SerializeSize = ArraySizeType.Int16)]
        public string Name { get; set; }

        [AoMember(1, SerializeSize = ArraySizeType.Int16)]
        public string MinQl { get; set; }

        [AoMember(2, SerializeSize = ArraySizeType.Int16)]
        public string MaxQl { get; set; }
    }
}
