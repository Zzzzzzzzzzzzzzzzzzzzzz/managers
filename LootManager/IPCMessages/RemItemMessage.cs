﻿using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LootManager.IPCMessages
{
    [AoContract((int)IPCOpcode.RemItem)]
    public class RemItemMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.RemItem;

        [AoMember(0)]
        public int ListNumberValue { get; set; }
    }
}
