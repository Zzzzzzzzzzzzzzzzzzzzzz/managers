﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LootManager.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 600,
        Stop = 601,
        AddItem = 602,
        RemItem = 603
    }
}
