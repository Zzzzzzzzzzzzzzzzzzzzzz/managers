﻿using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using AOSharp.Core.IPC;
using AOSharp.Common.GameData.UI;
using System.Threading.Tasks;
using AOSharp.Common.Unmanaged.DataTypes;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Common.Helpers;
using System.Runtime.InteropServices;
using System.Windows.Input;
using Newtonsoft.Json;
using System.Data;
using System.IO;
using AOSharp.Core.Movement;
using LootManager.IPCMessages;
using System.Diagnostics;

namespace LootManager
{
    public class LootManager : AOPluginEntry
    {
        private static Vector3 _currentCorpsePos = Vector3.Zero;
        private static string _currentCorpseName = string.Empty;

        public static List<Rule> Rules;

        private static IPCChannel IPCChannel;

        public static Config Config { get; private set; }

        protected Settings _settings;
        public static Settings _settingsItems;

        private static bool _init = false;
        private static bool _internalOpen = false;
        private static bool _working = false;
        private static bool _looting = false;

        public static bool _subbedUIEvents = false;

        private static bool _initBags = false;
        private static bool Toggle = false;

        public static float _point = 5f;
        public static float Interval;
        public static float Reset;

        private static double _packItemsTimer = Time.NormalTime;
        private static double _edgeCaseTimer = Time.NormalTime;
        private static double _workingTimer = Time.NormalTime;
        private static double _resetTimer = Time.NormalTime;

        public static Random random = new Random();

        private static List<Item> _invItems = new List<Item>();

        private static Identity Leader = Identity.None;

        private static List<string> _corpsesLooted = new List<string>();
        private static List<Vector3> _corpsesPositions = new List<Vector3>();

        public static Identity _corpseId = Identity.None;

        private static Window _infoWindow;
        private static Window _lootListWindow;

        private static View _lootListView;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("LootManager");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{Game.ClientInst}\\Config.json");
                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.AddItem, OnAddItemMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.RemItem, OnRemItemMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStopMessage);

                Game.OnUpdate += OnUpdate;
                Inventory.ContainerOpened += OnContainerOpened;

                Game.TeleportEnded += OnZoned;

                RegisterSettingsWindow("Loot Manager", "LootManagerSettingWindow.xml");

                LoadRules();

                _settings.AddVariable("Toggle", false);
                _settings.AddVariable("Relay", false);
                _settings.AddVariable("OpenBags", false);
                _settings.AddVariable("DisableOnEmpty", false);
                _settings.AddVariable("NoReplicas", false);
                _settings.AddVariable("CombatLooting", false);
                _settings.AddVariable("RemoveEntry", false);
                _settings.AddVariable("LeaveCorpseOpen", false);
                _settings.AddVariable("Delete", false);

                //_settings["Toggle"] = false;

                _settings.AddVariable("LootingSelection", (int)LootingSelection.Specific);
                _settings.AddVariable("MatchingSelection", (int)MatchingSelection.Exact);

                foreach (Item item in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory))
                    if (!_invItems.Contains(item))
                        _invItems.Add(item);

                Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    _settings["Toggle"] = !_settings["Toggle"].AsBool();
                    Toggle = !Toggle;

                    if (!Toggle)
                    {
                        IPCChannel.Broadcast(new StartMessage());
                    }
                    else { IPCChannel.Broadcast(new StopMessage()); }

                    Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
                });

                if (_settings["Toggle"].AsBool())
                    Toggle = true;

                Chat.WriteLine("Loot Manager loaded! Version 0.9.9.85");
                Chat.WriteLine("/lootmanager for settings.");
                Chat.WriteLine("Loot : Inventory set.");

                Interval = Config.CharSettings[Game.ClientInst].Interval;
                Reset = Config.CharSettings[Game.ClientInst].Reset;
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public override void Teardown()
        {
            SaveRules();
            SettingsController.CleanUp();
        }
        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void Interval_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Interval = e;
            Interval = e;
            Config.Save();
        }
        public static void Reset_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Reset = e;
            Reset = e;
            Config.Save();
        }

        private void OnZoned(object s, EventArgs e)
        {
            if (_settings["OpenBags"].AsBool() && !_init)
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(500);
                        foreach (Item _bag in Inventory.Items
                            .Where(c => c.UniqueIdentity.Type == IdentityType.Container
                                    && Inventory.Backpacks
                                        .Where(x => !x.IsOpen)
                                        .Select(x => x.Identity)
                                        .Contains(c.UniqueIdentity)))
                        {
                            _bag.Use();
                            _bag.Use();
                        }
                        _init = false;
                    });
            }
        }

        private void OnAddItemMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            AddItemMessage _addItemMsg = (AddItemMessage)msg;

            Rules.Add(new Rule(_addItemMsg.Name, _addItemMsg.MinQl, _addItemMsg.MaxQl));

            if (_lootListWindow != null && _lootListWindow.IsValid && _lootListWindow.IsVisible)
            {
                _lootListWindow.FindView("LootList", out MultiListView lootListView);
                _lootListWindow.FindView("ItemName", out TextInputView itemNameInput);
                _lootListWindow.FindView("MinQl", out TextInputView minQlInput);
                _lootListWindow.FindView("MaxQl", out TextInputView maxQlInput);

                if (itemNameInput != null && minQlInput != null && maxQlInput != null)
                {
                    lootListView.DeleteAllChildren();

                    int iEntry = 0;
                    foreach (Rule r in Rules)
                    {
                        View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                        entry.FindChild("ItemName", out TextView textView);

                        textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                        lootListView.AddChild(entry, false);
                        iEntry++;
                    }

                    itemNameInput.Text = "";
                    minQlInput.Text = "1";
                    maxQlInput.Text = "500";
                }
            }
        }
        private void OnStartMessage(int sender, IPCMessage msg)
        {
            if (Leader == Identity.None) { Leader = new Identity(IdentityType.SimpleChar, sender); }

            if (DynelManager.LocalPlayer.Identity == Leader || IsActiveWindow) { return; }

            _settings["Toggle"] = true;
            Toggle = true;

            SettingsController.CleanUp();

            Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity == Leader || IsActiveWindow) { return; }

            _settings["Toggle"] = false;
            Toggle = false;

            SettingsController.CleanUp();

            Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
        }

        private void OnRemItemMessage(int sender, IPCMessage msg)
        {
            if (DynelManager.LocalPlayer.Identity.Instance == sender) { return; }

            RemItemMessage _remItemMsg = (RemItemMessage)msg;

            if ((_remItemMsg.ListNumberValue + 1) >= 0 && Rules.Count() > 0 && (_remItemMsg.ListNumberValue + 1) <= Rules.Count())
            {
                Rules.RemoveAt(_remItemMsg.ListNumberValue);

                if (_lootListWindow != null && _lootListWindow.IsValid && _lootListWindow.IsVisible)
                {
                    _lootListWindow.FindView("LootList", out MultiListView lootListView);
                    _lootListWindow.FindView("ItemName", out TextInputView itemNameInput);
                    _lootListWindow.FindView("MinQl", out TextInputView minQlInput);
                    _lootListWindow.FindView("MaxQl", out TextInputView maxQlInput);
                    _lootListWindow.FindView("ListNumber", out TextInputView listNumberInput);

                    if (listNumberInput != null)
                    {
                        lootListView.DeleteAllChildren();

                        int iEntry = 0;
                        foreach (Rule r in Rules)
                        {
                            View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                            entry.FindChild("ItemName", out TextView textView);

                            textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                            lootListView.AddChild(entry, false);
                            iEntry++;
                        }

                        itemNameInput.Text = "";
                        minQlInput.Text = "1";
                        maxQlInput.Text = "500";
                    }
                }
            }
        }

        public Window[] _windows => new Window[] { _lootListWindow };

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\LootManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void HandleLootListButtonClick(object s, ButtonBase button)
        {
            Window window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();
            if (window != null)
            {
                if (window.Views.Contains(_lootListView)) { return; }

                _lootListView = View.CreateFromXml(PluginDir + "\\UI\\LootManagerLootListView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Loot List", XmlViewName = "LootManagerLootListView" }, _lootListView);

                window.FindView("LootList", out MultiListView lootListView);
                window.FindView("ItemName", out TextInputView itemNameInput);
                window.FindView("MinQl", out TextInputView minQlInput);
                window.FindView("MaxQl", out TextInputView maxQlInput);

                lootListView.DeleteAllChildren();

                int iEntry = 0;
                foreach (Rule r in Rules)
                {
                    View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                    entry.FindChild("ItemName", out TextView textView);

                    textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                    lootListView.AddChild(entry, false);
                    iEntry++;
                }

                itemNameInput.Text = "";
                minQlInput.Text = "1";
                maxQlInput.Text = "500";
            }
            else if (_lootListWindow == null || (_lootListWindow != null && !_lootListWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_lootListWindow, PluginDir, new WindowOptions() { Name = "Loot List", XmlViewName = "LootManagerLootListView", WindowSize = new Rect(0, 0, 210, 210) }, _lootListView, out var container);
                _lootListWindow = container;

                container.FindView("LootList", out MultiListView lootListView);
                container.FindView("ItemName", out TextInputView itemNameInput);
                container.FindView("MinQl", out TextInputView minQlInput);
                container.FindView("MaxQl", out TextInputView maxQlInput);

                lootListView.DeleteAllChildren();

                int iEntry = 0;
                foreach (Rule r in Rules)
                {
                    View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                    entry.FindChild("ItemName", out TextView textView);

                    textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                    lootListView.AddChild(entry, false);
                    iEntry++;
                }

                itemNameInput.Text = "";
                minQlInput.Text = "1";
                maxQlInput.Text = "500";
            }
        }

        private void HandleSetButtonClick(object s, ButtonBase button)
        {
            _invItems.Clear();

            foreach (Item item in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory))
                if (!_invItems.Contains(item))
                    _invItems.Add(item);

            Chat.WriteLine($"Loot : Inventory set.");
        }

        private void HandleAddButtonClick(object sender, ButtonBase e)
        {
            if (_lootListWindow != null)
            {
                _lootListWindow.FindView("LootList", out MultiListView lootListView);
                _lootListWindow.FindView("ItemName", out TextInputView itemNameInput);
                _lootListWindow.FindView("MinQl", out TextInputView minQlInput);
                _lootListWindow.FindView("MaxQl", out TextInputView maxQlInput);

                if (itemNameInput != null && !string.IsNullOrEmpty(itemNameInput.Text)
                    && minQlInput != null && !string.IsNullOrEmpty(minQlInput.Text)
                    && maxQlInput != null && !string.IsNullOrEmpty(maxQlInput.Text))
                {
                    if (_settings["Relay"].AsBool())
                    {
                        IPCChannel.Broadcast(new AddItemMessage()
                        {
                            Name = itemNameInput.Text,
                            MinQl = minQlInput.Text,
                            MaxQl = maxQlInput.Text
                        });
                    }

                    lootListView.DeleteAllChildren();
                    Rules.Add(new Rule(itemNameInput.Text, minQlInput.Text, maxQlInput.Text));
                    Rules = Rules.OrderBy(o => o.Name.ToUpper()).ToList();

                    int iEntry = 0;
                    foreach (Rule r in Rules)
                    {
                        View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                        entry.FindChild("ItemName", out TextView textView);

                        textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                        lootListView.AddChild(entry, false);
                        iEntry++;
                    }

                    itemNameInput.Text = "";
                    minQlInput.Text = "1";
                    maxQlInput.Text = "500";
                }
            }
        }

        private void HandleRemoveButtonClick(object sender, ButtonBase e)
        {
            if (_lootListWindow != null)
            {
                _lootListWindow.FindView("LootList", out MultiListView lootListView);
                _lootListWindow.FindView("ItemName", out TextInputView itemNameInput);
                _lootListWindow.FindView("MinQl", out TextInputView minQlInput);
                _lootListWindow.FindView("MaxQl", out TextInputView maxQlInput);
                _lootListWindow.FindView("ListNumber", out TextInputView listNumberInput);

                if (listNumberInput != null && !string.IsNullOrEmpty(listNumberInput.Text))
                {
                    if (int.TryParse(listNumberInput.Text, out int listNumberValue))
                    {
                        listNumberValue = listNumberValue - 1;

                        if (listNumberValue < 0) { return; }

                        if (_settings["Relay"].AsBool())
                        {
                            IPCChannel.Broadcast(new RemItemMessage()
                            {
                                ListNumberValue = listNumberValue
                            });
                        }

                        if (listNumberValue >= Rules.Count) { return; }

                        Rules.RemoveAt(listNumberValue);

                        lootListView.DeleteAllChildren();

                        int iEntry = 0;
                        foreach (Rule r in Rules)
                        {
                            View entry = View.CreateFromXml(PluginDir + "\\UI\\LootManagerItemEntry.xml");
                            entry.FindChild("ItemName", out TextView textView);

                            textView.Text = (iEntry + 1).ToString() + " - [" + r.MinQl.PadLeft(3, ' ') + "-" + r.MaxQl.PadLeft(3, ' ') + "  ] - " + r.Name;

                            lootListView.AddChild(entry, false);
                            iEntry++;
                        }

                        itemNameInput.Text = "";
                        minQlInput.Text = "1";
                        maxQlInput.Text = "500";
                    }
                }
            }
        }


        private static Backpack FindLootBagWithSpace()
        {
            foreach (Backpack _backpack in Inventory.Backpacks.Where(c => c.Name == "Loot" && c.IsOpen))
            {
                if (_backpack.Items.Count < 21)
                    return _backpack;
            }

            return null;
        }

        private static Backpack FindBagWithSpace(string str)
        {
            foreach (Backpack _backpack in Inventory.Backpacks.Where(c => str.Contains(c.Name) && c.Name != "" && c.IsOpen))
            {
                if (_backpack.Items.Count < 21)
                    return _backpack;
            }

            return null;
        }

        private void OnContainerOpened(object sender, Container container)
        {
            if (container.Identity.Type != IdentityType.Corpse)
                return;

            if (_internalOpen)
            {
                _looting = true;

                foreach (Item item in container.Items)
                {
                    if (Inventory.NumFreeSlots >= 1)
                    {
                        if (LootingSelection.Specific == (LootingSelection)_settings["LootingSelection"].AsInt32())
                        {
                            if (CheckRules(item))
                            {
                                if (_settings["NoReplicas"].AsBool())
                                {
                                    if (!Inventory.Items.Any(c => c.Name.ToLower() == item.Name.ToLower())
                                        && !Inventory.Backpacks.Any(c => c.Items.Select(x => x.Name.ToLower()).Contains(item.Name)))
                                        item.MoveToInventory();
                                }
                                else
                                    item.MoveToInventory();

                                if (_settings["RemoveEntry"].AsBool())
                                {
                                    Rule _rule = Rules.FirstOrDefault(c => item.Name.ToLower().Contains(c.Name.ToLower()));
                                    Rules.Remove(_rule);
                                }
                            }
                            else if (_settings["Delete"].AsBool())
                                item.Delete();
                        }
                        else if (LootingSelection.All == (LootingSelection)_settings["LootingSelection"].AsInt32())
                            item.MoveToInventory();
                    }
                }


                _corpsesPositions.Add(_currentCorpsePos);
                _corpsesLooted.Add(_currentCorpseName);

                if (!_settings["LeaveCorpseOpen"].AsBool())
                    Item.Use((Identity)container?.Identity);

                _looting = false;
                _internalOpen = false;
                _working = false;
            }
            //Edge case correction: Event is triggered before bool is set to true. #TIMINGISSUE
            else if (_working)
                Item.Use((Identity)container?.Identity);

        }

        private void OnUpdate(object sender, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            if (!_settings["Toggle"].AsBool()
                && (_corpsesPositions.Count() > 0 || _corpsesLooted.Count() > 0))
            {
                _corpsesPositions.Clear();
                _corpsesLooted.Clear();
            }

            if (_settings["OpenBags"].AsBool() && !_initBags)
            {
                List<Item> bags = Inventory.Items
                    .Where(c => c.UniqueIdentity.Type == IdentityType.Container)
                    .ToList();

                foreach (Item bag in bags)
                {
                    bag.Use();
                    bag.Use();
                }

                _initBags = true;
            }

            if (_settings["Toggle"].AsBool())
            {
                if (Rules.Count == 0 && _settings["DisableOnEmpty"].AsBool())
                {
                    _settings["Toggle"] = false;
                    Chat.WriteLine($"Loot list is empty.");
                    Chat.WriteLine($"Loot : false");
                }

                if (Reset > 0 && Time.NormalTime > _resetTimer + Reset
                    && (_corpsesPositions.Count() > 0 || _corpsesLooted.Count() > 0))
                {
                    _corpsesPositions.Clear();
                    _corpsesLooted.Clear();
                    _resetTimer = Time.NormalTime;
                }

                //Edge case correction: For when moving around and it lags and you get "You are too far away; please move closer!"
                if (Time.NormalTime > _edgeCaseTimer + 2f) 
                {
                    if (_working && !_looting && _internalOpen)
                    {
                        _working = false;
                        _internalOpen = false;
                    }

                    _edgeCaseTimer = Time.NormalTime;
                }

                if (Time.NormalTime > _packItemsTimer + random.Next(3, 11))
                {
                    foreach (Item itemtomove in Inventory.Items.Where(c => c.Slot.Type == IdentityType.Inventory))
                    {
                        if (itemtomove.Name.Contains("Cell Templates") || itemtomove.Name.Contains("Plasmid Cultures")
                                || itemtomove.Name.Contains("Mitochondria Samples") || itemtomove.Name.Contains("Protein Mapping Data")
                                || itemtomove.Name.Contains("Soul Capsule"))
                            itemtomove?.Use();

                        if (_invItems.Contains(itemtomove)) { continue; }

                        if (LootingSelection.All == (LootingSelection)_settings["LootingSelection"].AsInt32())
                        {
                            Backpack _bagLoot = FindLootBagWithSpace();

                            if (_bagLoot != null)
                                itemtomove?.MoveToContainer(_bagLoot);
                        }
                        else
                        {
                            Backpack _bag = FindBagWithSpace(itemtomove?.Name);

                            if (_bag != null)
                                itemtomove?.MoveToContainer(_bag);
                            else
                            {
                                Backpack _bagLoot = FindLootBagWithSpace();

                                if (_bagLoot != null)
                                    itemtomove?.MoveToContainer(_bagLoot);
                            }
                        }
                    }

                    _packItemsTimer = Time.NormalTime;
                }

                if (!_working && !_looting && !_internalOpen && Time.NormalTime > _workingTimer + Interval)
                {
                    if (!_settings["CombatLooting"].AsBool() 
                        && DynelManager.LocalPlayer.FightingTarget != null)
                    {
                        _workingTimer = Time.NormalTime;
                        return;
                    }

                    Corpse corpse = DynelManager.Corpses
                        .Where(c => !c.Name.Contains("Coccoon")
                            && !c.Name.Contains("Cocoon")
                            && c.DistanceFrom(DynelManager.LocalPlayer) <= 7f
                            && CanLoot(c))
                        .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                        .FirstOrDefault();

                    if (corpse != null)
                    {
                        _internalOpen = true;
                        _working = true;

                        _currentCorpsePos = corpse.Position;
                        _currentCorpseName = corpse.Name.Remove(0, 10);

                        corpse.Open();
                        _edgeCaseTimer = Time.NormalTime;
                    }

                    _workingTimer = Time.NormalTime;
                }
            }

            #region UI Update

            if (_subbedUIEvents && (bool)!_windows?.Any(c => c != null && c.IsValid)
                && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                if (window.FindView("AddButton", out Button addButton))
                {
                    addButton.Tag = window;
                    addButton.Clicked = HandleAddButtonClick;
                }

                if (window.FindView("RemoveButton", out Button removeButton))
                {
                    removeButton.Tag = window;
                    removeButton.Clicked = HandleRemoveButtonClick;
                }
            }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("IntervalBox", out TextInputView intervalInput);
                SettingsController.settingsWindow.FindView("ResetBox", out TextInputView resetInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (intervalInput != null && !string.IsNullOrEmpty(intervalInput.Text))
                    if (float.TryParse(intervalInput.Text, out float intervalValue)
                        && Config.CharSettings[Game.ClientInst].Interval != intervalValue)
                        Config.CharSettings[Game.ClientInst].Interval = intervalValue;
                if (resetInput != null && !string.IsNullOrEmpty(resetInput.Text))
                    if (float.TryParse(resetInput.Text, out float resetValue)
                        && Config.CharSettings[Game.ClientInst].Reset != resetValue)
                        Config.CharSettings[Game.ClientInst].Reset = resetValue;

                if (SettingsController.settingsWindow.FindView("LootManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("LootListButton", out Button lootListButton))
                {
                    lootListButton.Tag = SettingsController.settingsWindow;
                    lootListButton.Clicked = HandleLootListButtonClick;
                }

                if (SettingsController.settingsWindow.FindView("SetButton", out Button setButton))
                {
                    setButton.Tag = SettingsController.settingsWindow;
                    setButton.Clicked = HandleSetButtonClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    if (IsActiveWindow)
                    {
                        if (Leader != Identity.None) { Leader = DynelManager.LocalPlayer.Identity; }              

                        if (DynelManager.LocalPlayer.Identity == Leader) { IPCChannel.Broadcast(new StartMessage()); }

                        Toggle = true;

                        Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
                    }
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    if (IsActiveWindow)
                    {
                        if (Leader != Identity.None) { Leader = DynelManager.LocalPlayer.Identity; }

                        if (DynelManager.LocalPlayer.Identity == Leader) { IPCChannel.Broadcast(new StopMessage()); }

                        Toggle = false;

                        Chat.WriteLine($"Loot : {_settings["Toggle"].AsBool()}");
                    }
                }
            }

            #endregion
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].IntervalChangedEvent += Interval_Changed;
                Config.CharSettings[Game.ClientInst].ResetChangedEvent += Reset_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].IntervalChangedEvent += Interval_Changed;
                Config.CharSettings[Game.ClientInst].ResetChangedEvent += Reset_Changed;
            }
        }

        public static bool CanLoot(Corpse corpse)
        {
            if (corpse.DistanceFrom(DynelManager.LocalPlayer) <= 7f
                && (corpse?.Container.IsOpen == false
                    || (corpse?.Container.IsOpen == true
                        && (!_corpsesLooted.Contains(corpse?.Name.Remove(0, 10))
                            || !_corpsesPositions.Contains((Vector3)corpse?.Position))))) { return true; }

            return false;
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void LoadRules()
        {
            Rules = new List<Rule>();

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}");

            string filename = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}\\Rules.json";
            if (File.Exists(filename))
            {
                List<Rule> scopedRules = new List<Rule>();
                string rulesJson = File.ReadAllText(filename);
                scopedRules = JsonConvert.DeserializeObject<List<Rule>>(rulesJson);
                foreach (Rule rule in scopedRules)
                {
                    Rules.Add(rule);
                }
            }

            Rules = Rules.OrderBy(o => o.Name.ToUpper()).ToList();
        }

        private void SaveRules()
        {
            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager");

            if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}"))
                Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}");

            string filename = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\LootManager\\{DynelManager.LocalPlayer.Name}\\Rules.json";
            string rulesJson = JsonConvert.SerializeObject(Rules);
            File.WriteAllText(filename, rulesJson);
        }

        public bool CheckRules(Item item)
        {
            foreach (Rule rule in Rules)
            {
               if (MatchingSelection.Exact == (MatchingSelection)_settings["MatchingSelection"].AsInt32())
               {
                    if (item.Name.ToUpper() == rule.Name.ToUpper() &&
                        item.QualityLevel >= Convert.ToInt32(rule.MinQl) &&
                        item.QualityLevel <= Convert.ToInt32(rule.MaxQl)) { return true; }
               }
               else if (MatchingSelection.Wildcard == (MatchingSelection)_settings["MatchingSelection"].AsInt32())
               {
                    if (item.Name.ToUpper().Contains(rule.Name.ToUpper()) &&
                        item.QualityLevel >= Convert.ToInt32(rule.MinQl) &&
                        item.QualityLevel <= Convert.ToInt32(rule.MaxQl)) { return true; }
               }
            }

            return false;
        }

    }

    public enum LootingSelection
    {
        Specific, All
    }
    public enum MatchingSelection
    {
        Exact, Wildcard
    }
}
