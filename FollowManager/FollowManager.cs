﻿using System;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System.Runtime.InteropServices;
using AOSharp.Common.GameData.UI;
using AOSharp.Core.Inventory;
using System.Threading.Tasks;
using AOSharp.Core.Misc;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.GameData;

namespace FollowManager
{
    public class FollowManager : AOPluginEntry
    {
        public static Config Config { get; private set; }

        private static string FollowPlayer;
        private static string NavFollowIdentity;
        private static int NavFollowDistance;

        private static double _timerTick;
        private static double _timerFollow;
        public static double _sitUpdateTimer;

        private static bool Toggle = false;
        public static bool _initSit = false;
        public static bool _init = false;
        public static bool _subbedUIEvents = false;

        private bool _internalZone = false;
        private double _zonedTimer;

        //public static Spell _isCastingSpellDummy;

        public static float Tick = 0;

        public static double _castTimer;
        public static double _castRecharge;
        public static bool _casting = false;

        public static Item _kit;

        public static Pet _pet;
        public static Pet _hinderedPet;

        public static Window _infoWindow;

        public static View _infoView;

        public static Settings _settings;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("FollowManager");
            PluginDir = pluginDir;

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\FollowManager\\{Game.ClientInst}\\Config.json");

            RegisterSettingsWindow("Follow Manager", "FollowManagerSettingsView.xml");

            Game.OnUpdate += OnUpdate;
            Network.N3MessageReceived += Network_N3MessageReceived;
            Game.TeleportEnded += OnZoned;
            Network.N3MessageSent += Network_N3MessageSent;

            _settings.AddVariable("Toggle", false);
            _settings.AddVariable("Sitting", false);
            _settings.AddVariable("Jumping", false);
            _settings.AddVariable("WhomPahs", false);

            Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["Toggle"] = !_settings["Toggle"].AsBool();
                Toggle = !Toggle;
                Chat.WriteLine($"Follow : {_settings["Toggle"].AsBool()}");
            });

            if (_settings["Toggle"].AsBool())
                Toggle = true;

            //IsCasting Bandaid
            //_isCastingSpellDummy = Spell.List.FirstOrDefault(c => c.Id == 223372);

            _settings.AddVariable("FollowSelection", (int)FollowSelection.None);

            Chat.WriteLine("FollowManager Loaded! Version: 0.9.9.87");
            Chat.WriteLine("/followmanager for settings.");


            FollowPlayer = Config.CharSettings[Game.ClientInst].FollowPlayer;
            NavFollowIdentity = Config.CharSettings[Game.ClientInst].NavFollowIdentity;
            NavFollowDistance = Config.CharSettings[Game.ClientInst].NavFollowDistance;
            Tick = Config.CharSettings[Game.ClientInst].Tick;
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        public static void FollowPlayer_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].FollowPlayer = e;
            FollowPlayer = e;
            Config.Save();
        }
        public static void NavFollowIdentity_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].NavFollowIdentity = e;
            NavFollowIdentity = e;
            Config.Save();
        }
        public static void NavFollowDistance_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].NavFollowDistance = e;
            NavFollowDistance = e;
            Config.Save();
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\FollowManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }


        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F3) && !_init)
            //{
            //    _init = true;

            //    Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\FollowManager\\{Game.ClientInst}\\Config.json");

            //    SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 250, 300), "Follow Manager", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

            //    if (SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsVisible)
            //    {
            //        foreach (string settingsName in SettingsController.settingsWindows.Keys.Where(x => x.Contains("Follow Manager")))
            //        {
            //            SettingsController.AppendSettingsTab(settingsName, SettingsController.settingsWindow);

            //            SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
            //            SettingsController.settingsWindow.FindView("FollowNamedCharacter", out TextInputView followBox);
            //            SettingsController.settingsWindow.FindView("FollowNamedIdentity", out TextInputView navFollowBox);
            //            SettingsController.settingsWindow.FindView("NavFollowDistanceBox", out TextInputView navFollowDistanceBox);

            //            if (channelInput != null)
            //                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
            //            if (followBox != null)
            //                followBox.Text = $"{Config.CharSettings[Game.ClientInst].FollowPlayer}";
            //            if (navFollowBox != null)
            //                navFollowBox.Text = $"{Config.CharSettings[Game.ClientInst].NavFollowIdentity}";
            //            if (navFollowDistanceBox != null)
            //                navFollowDistanceBox.Text = $"{Config.CharSettings[Game.ClientInst].NavFollowDistance}";
            //        }
            //    }

            //    _init = false;
            //}

            if (Game.IsZoning || DynelManager.LocalPlayer.IsFalling)
                return;

            if (Toggle && Time.NormalTime > _timerTick + Tick)
            {
                #region Whom-Pahs and Doors

                if (_settings["WhomPahs"].AsBool())
                {
                    foreach (Dynel dynel in DynelManager.AllDynels.Where(c => c.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 5f))
                    {
                        if (dynel?.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 4.2f
                            && (bool)dynel?.Name.Contains("Whom-Pah to"))
                        {
                            if (!_internalZone && Time.NormalTime > _zonedTimer + 5f)
                            {
                                _internalZone = true;
                                DynelManager.LocalPlayer.Position = dynel.Position;
                                MovementController.Instance.SetMovement(MovementAction.Update);
                                MovementController.Instance.SetMovement(MovementAction.Update);
                                MovementController.Instance.SetMovement(MovementAction.ForwardStart);
                                MovementController.Instance.SetMovement(MovementAction.ForwardStop);
                            }
                        }
                        //else if (dynel?.Position.Distance2DFrom(DynelManager.LocalPlayer.Position) <= 1.2f
                        //    && dynel?.Identity.Type == IdentityType.Door && !Playfield.IsDungeon 
                        //    && Time.NormalTime > _zonedTimer + 7f)
                        //{
                        //    _internalZone = true;
                        //    _zonedTimer = Time.NormalTime;
                        //    DynelManager.LocalPlayer.Position = dynel.Position;
                        //    MovementController.Instance.SetMovement(MovementAction.Update);
                        //    MovementController.Instance.SetMovement(MovementAction.Update);
                        //}
                    }
                }

                #endregion

                if (_settings["Sitting"].AsBool())
                    ListenerSit();

                if (FollowSelection.NavFollow == (FollowSelection)_settings["FollowSelection"].AsInt32())
                {
                    Dynel _identity = DynelManager.AllDynels
                        .FirstOrDefault(c => !string.IsNullOrEmpty(NavFollowIdentity)
                            && !c.Flags.HasFlag(CharacterFlags.Pet)
                            && c.Name.ToLower() == NavFollowIdentity.ToLower());

                    if (_identity != null)
                    {
                        if (MovementController.Instance.IsNavigating && DynelManager.LocalPlayer.DistanceFrom(_identity) <= NavFollowDistance)
                        { MovementController.Instance.Halt(); }

                        if (DynelManager.LocalPlayer.DistanceFrom(_identity) > NavFollowDistance
                            && !_initSit && Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && c.IsReady)) { MovementController.Instance.SetDestination(_identity.Position); }
                    }
                }
                else if (FollowSelection.NamedFollow == (FollowSelection)_settings["FollowSelection"].AsInt32() && Time.NormalTime > _timerFollow + 2f)
                {
                    SimpleChar _identity = DynelManager.Players
                        .FirstOrDefault(c => !string.IsNullOrEmpty(FollowPlayer)
                            && !c.Flags.HasFlag(CharacterFlags.Pet)
                            && c.Name.ToLower() == FollowPlayer.ToLower() && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > 4.5f);

                    if (_identity != null) { NamedFollow(_identity); }

                    _timerFollow = Time.NormalTime;
                }

                _timerTick = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);
                SettingsController.settingsWindow.FindView("FollowNamedCharacter", out TextInputView followInput);
                SettingsController.settingsWindow.FindView("FollowNamedIdentity", out TextInputView navFollowInput);
                SettingsController.settingsWindow.FindView("NavFollowDistanceBox", out TextInputView navFollowDistanceInput);

                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (followInput != null && !string.IsNullOrEmpty(followInput.Text))
                    if (Config.CharSettings[Game.ClientInst].FollowPlayer != followInput.Text)
                        Config.CharSettings[Game.ClientInst].FollowPlayer = followInput.Text;
                if (navFollowInput != null && !string.IsNullOrEmpty(navFollowInput.Text))
                    if (Config.CharSettings[Game.ClientInst].NavFollowIdentity != navFollowInput.Text)
                        if (Config.CharSettings[Game.ClientInst].NavFollowIdentity != navFollowInput.Text)
                            Config.CharSettings[Game.ClientInst].NavFollowIdentity = navFollowInput.Text;
                if (navFollowDistanceInput != null && !string.IsNullOrEmpty(navFollowDistanceInput.Text))
                    if (int.TryParse(navFollowDistanceInput.Text, out int navFollowDistanceValue)
                        && Config.CharSettings[Game.ClientInst].NavFollowDistance != navFollowDistanceValue)
                        Config.CharSettings[Game.ClientInst].NavFollowDistance = navFollowDistanceValue;

                if (SettingsController.settingsWindow.FindView("FollowManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Toggle = true;
                    Chat.WriteLine($"Follow : {_settings["Toggle"].AsBool()}");
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Toggle = false;
                    Chat.WriteLine($"Follow : {_settings["Toggle"].AsBool()}");
                }
            }

            #endregion
        }

        private void OnZoned(object s, EventArgs e)
        {
            _internalZone = false;
            _zonedTimer = Time.NormalTime;
        }
        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.CastNano)
                {
                    if (Spell.Find(charActionMsg.Parameter2, out Spell _currentNano))
                    {
                        _casting = true;
                        _castTimer = Time.NormalTime;
                        _castRecharge = _currentNano.AttackDelay;
                    }
                }
            }
        }
        private void Network_N3MessageReceived(object s, N3Message n3Msg)
        {
            if (n3Msg.Identity == DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharDCMove)
            {
                if (_settings["Jumping"].AsBool())
                {
                    CharDCMoveMessage charDCMoveMsg = (CharDCMoveMessage)n3Msg;

                    if (charDCMoveMsg.MoveType == MovementAction.JumpStart)
                    {
                        if (FollowSelection.NavFollow == (FollowSelection)_settings["FollowSelection"].AsInt32())
                        {
                            Dynel _identity = DynelManager.AllDynels
                                .FirstOrDefault(c => !string.IsNullOrEmpty(NavFollowIdentity)
                                    && !c.Flags.HasFlag(CharacterFlags.Pet)
                                    && c.Name.ToLower() == NavFollowIdentity.ToLower());

                            if (MovementController.Instance.IsNavigating)
                                MovementController.Instance.Halt();

                            if (_identity != null && charDCMoveMsg.Identity == _identity.Identity)
                            {
                                MovementController.Instance.SetMovement(MovementAction.JumpStart);

                                if (_identity.IsMoving || DynelManager.LocalPlayer.IsMoving) { MovementController.Instance.SetMovement(MovementAction.ForwardStart); }
                            }
                        }
                        else if (FollowSelection.NamedFollow == (FollowSelection)_settings["FollowSelection"].AsInt32())
                        {
                            Dynel _identity = DynelManager.AllDynels
                                .FirstOrDefault(c => !string.IsNullOrEmpty(FollowPlayer)
                                    && !c.Flags.HasFlag(CharacterFlags.Pet)
                                    && c.Name.ToLower() == FollowPlayer.ToLower());

                            if (_identity != null && charDCMoveMsg.Identity == _identity.Identity) { MovementController.Instance.SetMovement(MovementAction.JumpStart); }
                        }
                    }
                }
            }
        }

        private void ListenerSit()
        {
            bool shouldHalt = Extensions.PetNanoLow() || Extensions.PetHindered();
            bool canSitKit = Extensions.CanUseSitKit();

            if (shouldHalt) { return; }

            if (_initSit)
            {
                if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                {
                    if (Extensions.InCombat() || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66))
                    {
                        _initSit = false;
                        MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                    }
                }
                else { _initSit = false; }
            }
            else if (!Extensions.InCombat() && !LocalCooldown.IsOnCooldown(Stat.Treatment) && canSitKit)
            {
                if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
                {
                    _initSit = true;
                    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                }
            }

            //if (Extensions.CanUseSitKit())
            //{
            //    if (_initSit)
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit && !Extensions.PetNanoLow() && !Extensions.PetHindered())
            //        {
            //            if (Extensions.InCombat() || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66))
            //            {
            //                _initSit = false;
            //                MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            }
            //        }
            //        else { _initSit = false; }
            //    }
            //    else if (!Extensions.InCombat() && !LocalCooldown.IsOnCooldown(Stat.Treatment) && !Extensions.PetNanoLow() && !Extensions.PetHindered())
            //    {
            //        if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //        {
            //            _initSit = true;
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //        }
            //    }
            //}

            //if (Extensions.CanUseSitKit())
            //{
            //    if (_initSit)
            //    {
            //        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
            //        {
            //            if (Extensions.InCombat() 
            //                || (DynelManager.LocalPlayer.NanoPercent >= 66 && DynelManager.LocalPlayer.HealthPercent >= 66 && !Extensions.PetNanoLow() && !Extensions.PetHindered()))
            //            {
            //                _initSit = false;
            //                MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            }
            //        }
            //        else { _initSit = false; }
            //    }
            //    else if (!Extensions.InCombat() && !LocalCooldown.IsOnCooldown(Stat.Treatment))
            //    {
            //        if (Extensions.PetNanoLow() || Extensions.PetHindered()) { return; }

            //        if (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66)
            //        {
            //            _initSit = true;
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //        }
            //    }
            //}

            //if (_initSit == false && Extensions.CanUseSitKit()
            //    && (DynelManager.LocalPlayer.NanoPercent < 66 || DynelManager.LocalPlayer.HealthPercent < 66))
            //{
            //    Task.Factory.StartNew(
            //        async () =>
            //        {
            //            _initSit = true;
            //            await Task.Delay(400);
            //            MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
            //            await Task.Delay(1800);
            //            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
            //            await Task.Delay(400);
            //            _initSit = false;
            //        });
            //}
        }

        private void NamedFollow(Dynel dynel)
        {
            FollowTargetMessage n3Msg = new FollowTargetMessage()
            {
                Info = new FollowTargetMessage.TargetInfo()
                {
                    Target = dynel.Identity
                },
                Type = FollowTargetType.Target
            };
            Network.Send(n3Msg);
            MovementController.Instance.SetMovement(MovementAction.Update);
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].FollowPlayerChangedEvent -= FollowPlayer_Changed;
                Config.CharSettings[Game.ClientInst].NavFollowIdentityChangedEvent -= NavFollowIdentity_Changed;
                Config.CharSettings[Game.ClientInst].NavFollowDistanceChangedEvent -= NavFollowDistance_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].FollowPlayerChangedEvent += FollowPlayer_Changed;
                Config.CharSettings[Game.ClientInst].NavFollowIdentityChangedEvent += NavFollowIdentity_Changed;
                Config.CharSettings[Game.ClientInst].NavFollowDistanceChangedEvent += NavFollowDistance_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        public enum FollowSelection
        {
            None, NamedFollow, NavFollow
        }
    }
}
