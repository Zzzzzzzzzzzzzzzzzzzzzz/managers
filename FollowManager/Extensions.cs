﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.Inventory;
using AOSharp.Core;
using static FollowManager.FollowManager;

namespace FollowManager
{
    public static class Extensions
    {
        public static bool InCombat()
        {
            if (DynelManager.LocalPlayer.FightingTarget != null
                || DynelManager.LocalPlayer.IsAttacking
                || DynelManager.LocalPlayer.IsAttackPending
                || DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0) { return true; }

            if (Team.IsInTeam)
            {
                return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && Team.Members.Select(m => m.Name).Contains(c.FightingTarget?.Name));
            }

            return DynelManager.Characters
                    .Any(c => c.Health > 0
                        && c.FightingTarget != null
                        && c.FightingTarget?.Name == DynelManager.LocalPlayer.Name);
        }
        public static bool FindKit(out Item item)
        {
            return (item = Inventory.Items.FirstOrDefault(c => Constants.Kits.Contains(c.Id))) != null;
        }
        public static bool TryGetHealPet(out Pet healPet)
        {
            return (healPet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Type == PetType.Heal)) != null;
        }

        public static bool PetNanoLow()
        {
            Pet _healPet;

            return (TryGetHealPet(out _healPet) && (_healPet.Character?.Nano / PetMaxNanoPool(_healPet) * 100 <= 55
                    && _healPet.Character?.Nano != 10
                    && DynelManager.LocalPlayer.DistanceFrom(_healPet.Character) < 10f && _healPet.Character?.IsInLineOfSight == true));
        }

        public static float PetMaxNanoPool(Pet _healPet)
        {
            if (_healPet.Character.Level == 215)
                return 5803;
            else if (_healPet.Character.Level == 192)
                return 13310;
            else if (_healPet.Character.Level == 169)
                return 11231;
            else if (_healPet.Character.Level == 146)
                return 9153;
            else if (_healPet.Character.Level == 123)
                return 7169;
            else if (_healPet.Character.Level == 99)
                return 5327;
            else if (_healPet.Character.Level == 77)
                return 3807;
            else if (_healPet.Character.Level == 55)
                return 2404;
            else if (_healPet.Character.Level == 33)
                return 1234;
            else if (_healPet.Character.Level == 14)
                return 414;

            return 0;
        }

        public static bool PetHindered()
        {
            return DynelManager.LocalPlayer.Pets
                .Any(c => c.Character?.Buffs.Contains(NanoLine.Root) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOERoot) == true
                     || c.Character?.Buffs.Contains(NanoLine.Snare) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOESnare) == true
                     || c.Character?.Buffs.Contains(NanoLine.Mezz) == true
                     || c.Character?.Buffs.Contains(NanoLine.AOEMezz) == true);
        }

        public static bool Mezzed()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Mezz)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOEMezz);
        }

        public static bool Snared()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Snare)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOESnare);
        }
        public static bool Rooted()
        {
            return DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot);
        }
        public static bool IsCasting()
        {
            if (_casting)
            {
                if (Time.NormalTime > _castTimer + _castRecharge)
                {
                    _casting = false;
                }
                else { return true; }
            }

            return false;
        }

        public static bool CanUseSitKit()
        {
            if (!IsCasting() && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving)
            {
                if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

                List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

                if ((bool)sitKits?.Any()) { return true; }
            }

            return false;
        }

        //public static bool CanUseSitKit()
        //{
        //    if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)
        //        && DynelManager.LocalPlayer.Health > 0 && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning
        //        && !DynelManager.LocalPlayer.Buffs.Contains(280488) && !InCombat())
        //    {
        //        if (!Spell.List.Any(c => (c.Id == 223372 || c.Id == 287046) && c.IsReady)) { return false; }

        //        if (Inventory.Items.Any(c => c.Id == 297274)) { return true; }

        //        List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

        //        if (sitKits.Any())
        //        {
        //            foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
        //            {
        //                int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

        //                if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
        //                    return true;
        //            }
        //        }
        //    }

        //    return false;
        //}

        //public static bool CanUseSitKit(out Item _kit)
        //{
        //    if (DynelManager.LocalPlayer.Buffs.Contains(280488) || !Spell.List.Any(c => c.IsReady))
        //    {
        //        _kit = null;
        //        return false;
        //    }

        //    if (DynelManager.LocalPlayer.Health > 0 && !InCombat()
        //            && !DynelManager.LocalPlayer.IsMoving && !Game.IsZoning)
        //    {
        //        if (Inventory.Find(297274, out _kit)) { return true; }

        //        List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

        //        if (!sitKits.Any()) { return false; }

        //        foreach (Item sitKit in sitKits.OrderBy(x => x.QualityLevel))
        //        {
        //            int skillReq = (sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f));

        //            if (DynelManager.LocalPlayer.GetStat(Stat.FirstAid) >= skillReq || DynelManager.LocalPlayer.GetStat(Stat.Treatment) >= skillReq)
        //            {
        //                _kit = sitKit;
        //                return true;
        //            }
        //        }
        //    }

        //    _kit = null;
        //    return false;
        //}
    }
}
