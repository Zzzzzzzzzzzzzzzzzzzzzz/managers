﻿using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelpManager.IPCMessages
{
    [AoContract((int)IPCOpcode.SyncTarget)]
    public class SyncTargetMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.SyncTarget;

        [AoMember(0)]
        public SimpleChar Target { get; set; }
    }
}
