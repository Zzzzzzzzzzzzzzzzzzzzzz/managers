﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using AOSharp.Core.IPC;
using System;
using AOSharp.Common.GameData.UI;
using ResearchManager.IPCMessages;

namespace ResearchManager
{
    public class ResearchManager : AOPluginEntry
    {
        public static Settings _settings;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static bool _subbedUIEvents = false;

        private static double Tick;
        private static bool Toggle = false;

        private static int ResearchPercentage = 0;

        private static Window infoWindow;

        private static string _perkName = N3EngineClientAnarchy.GetPerkName(DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal));
        private static int _perkID = DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal);

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("ResearchManager");
            PluginDir = pluginDir;

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\ResearchManager\\{Game.ClientInst}\\Config.json");
            IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

            IPCChannel.RegisterCallback((int)IPCOpcode.SetPercentage, OnSetPercentageMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStartMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Start, OnStopMessage);

            Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
            Config.CharSettings[Game.ClientInst].ResearchPercentageChangedEvent += ResearchPercentage_Changed;

            Game.OnUpdate += OnUpdate;

            _settings.AddVariable("Toggle", false);

            _settings.AddVariable("TypeSelection", (int)TypeSelection.Self);

            RegisterSettingsWindow("Research Manager", $"ResearchManagerSettingsView.xml");

            Chat.RegisterCommand("research", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["Toggle"] = !_settings["Toggle"].AsBool();

                if (!Toggle)
                {
                    IPCChannel.Broadcast(new StartMessage());
                }
                else { IPCChannel.Broadcast(new StopMessage()); }

                Chat.WriteLine($"Research : {_settings["Toggle"].AsBool()}");
            });

            if (_settings["Toggle"].AsBool())
                Toggle = true;

            Chat.WriteLine("Research Manager Loaded! Version 0.9.9.84");
            Chat.WriteLine("/researchmanager for settings.");

            ResearchPercentage = Config.CharSettings[Game.ClientInst].ResearchPercentage;
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        private void OnStartMessage(int sender, IPCMessage msg)
        {
            Toggle = true;

            Chat.WriteLine($"Research : {_settings["Toggle"].AsBool()}");
        }

        private void OnStopMessage(int sender, IPCMessage msg)
        {
            Toggle = false;

            Chat.WriteLine($"Research : {_settings["Toggle"].AsBool()}");
        }
        public static void ResearchPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].ResearchPercentage = e;
            ResearchPercentage = e;
            Config.Save();
        }

        private void OnSetPercentageMessage(int sender, IPCMessage msg)
        {
            SetPercentageMessage setPercentage = (SetPercentageMessage)msg;

            Chat.WriteLine($"Research % : {setPercentage.Percentage}");

            DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchLevel, setPercentage.Percentage);
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\ResearchManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }
        private void HandleSetButtonClick(object s, ButtonBase button)
        {
            if (TypeSelection.All == (TypeSelection)_settings["TypeSelection"].AsInt32())
            {
                Chat.WriteLine($"Research % : {ResearchPercentage}");
                DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchLevel, ResearchPercentage);
                IPCChannel.Broadcast(new SetPercentageMessage()
                {
                    Percentage = ResearchPercentage
                });
            }
            else
            {
                Chat.WriteLine($"Research % : {ResearchPercentage}");
                DynelManager.LocalPlayer.SetStat(Stat.PersonalResearchLevel, ResearchPercentage);
            }
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F1))
            //{
            //    if (SettingsController.settingsWindow == null)
            //        SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 280, 280), "Research Manager", "Settings", WindowStyle.Default, WindowFlags.None);

            //    if (SettingsController.settingsWindow != null && SettingsController.settingsWindow?.IsVisible == false)
            //    {
            //        SettingsController.AppendSettingsTab("Research Manager", SettingsController.settingsWindow);
            //    }
            //}

            if (Game.IsZoning) { return; }

            if (_settings["Toggle"].AsBool() && Time.NormalTime > Tick + 5f)
            {
                var availableGoals = Research.Goals
                    .Where(goal => goal.Available)
                    .OrderBy(goal => GetPerkLevel(goal.ResearchId))
                    .ThenByDescending(goal => N3EngineClientAnarchy.GetPerkProgress((uint)goal.ResearchId));

                if (availableGoals.Count() > 0)
                {
                    ResearchGoal goal = availableGoals.First();

                    if (DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal) != goal.ResearchId)
                    {
                        Research.Train(goal.ResearchId);
                    }
                }

                //if (Research.Completed.Contains((uint)DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal))
                //    || DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal) == 0
                //    || Research.Goals.Any(c => c.GetLineName() == N3EngineClientAnarchy.GetPerkName(DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal))
                //        && DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal) == c.ResearchId && !c.Available))
                //{
                //    ResearchGoal _next = Research.Goals
                //        .Where(c => c.GetLineName() != N3EngineClientAnarchy.GetPerkName(DynelManager.LocalPlayer.GetStat(Stat.PersonalResearchGoal))
                //            && c.Available
                //            && c.ResearchId >= 0)
                //        .OrderBy(c => c.ResearchId)
                //        .FirstOrDefault();

                //    Research.Train(_next.ResearchId);
                //}
                //else if (Research.Goals.Any(c => !c.Available))
                //{
                //    _settings["Toggle"] = false;
                //    Chat.WriteLine("Finished.");
                //}

                Tick = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("ResearchPercentageBox", out TextInputView researchInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text)
                    && int.TryParse(channelInput.Text, out int channelValue)
                    && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                    Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (researchInput != null && !string.IsNullOrEmpty(researchInput.Text)
                    && int.TryParse(researchInput.Text, out int researchValue)
                    && Config.CharSettings[Game.ClientInst].ResearchPercentage != researchValue)
                    Config.CharSettings[Game.ClientInst].ResearchPercentage = researchValue;

                if (SettingsController.settingsWindow.FindView("ResearchManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (SettingsController.settingsWindow.FindView("SetResearchPercentageButton", out Button setButton))
                {
                    setButton.Tag = SettingsController.settingsWindow;
                    setButton.Clicked = HandleSetButtonClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Toggle = true;

                    IPCChannel.Broadcast(new StartMessage());

                    Chat.WriteLine($"Research : {_settings["Toggle"].AsBool()}");
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Toggle = false;

                    IPCChannel.Broadcast(new StopMessage());

                    Chat.WriteLine($"Research : {_settings["Toggle"].AsBool()}");
                }
            }

            #endregion
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].ResearchPercentageChangedEvent -= ResearchPercentage_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].ResearchPercentageChangedEvent += ResearchPercentage_Changed;
            }
        }

        private int GetPerkLevel(int perkId)
        {
            return (perkId % 10) + 1;
        }

        public enum TypeSelection
        {
            Self, All
        }
    }
}
