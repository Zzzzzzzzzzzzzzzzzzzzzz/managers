﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResearchManager.IPCMessages
{
    public enum IPCOpcode
    {
        Start = 500,
        Stop = 501,
        SetPercentage = 502
    }
}
