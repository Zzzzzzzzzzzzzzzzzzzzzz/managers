﻿using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResearchManager.IPCMessages
{
    [AoContract((int)IPCOpcode.SetPercentage)]
    public class SetPercentageMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.SetPercentage;

        [AoMember(0)]
        public int Percentage { get; set; }
    }
}
