﻿using System;
using System.Linq;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;

namespace AssistManager
{
    public class AssistManager : AOPluginEntry
    {
        public static Config Config { get; private set; }

        protected Settings _settings;

        private static string AssistPlayer;

        private static double _timer;

        private static bool Toggle = false;
        public static float Tick = 0;

        public static bool _subbedUIEvents = false;

        public static Window _infoWindow;

        public static View _infoView;

        public static string PluginDir;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("AssistManager");
            PluginDir = pluginDir;

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AssistManager\\{Game.ClientInst}\\Config.json");

            RegisterSettingsWindow("Assist Manager", "AssistManagerSettingsView.xml");

            Game.OnUpdate += OnUpdate;

            _settings.AddVariable("Toggle", false);

            _settings.AddVariable("ModeSelection", (int)ModeSelection.None);

            Chat.RegisterCommand("manager", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["Toggle"] = !_settings["Toggle"].AsBool();
                Toggle = !Toggle;
                Chat.WriteLine($"Assist : {_settings["Toggle"].AsBool()}");
            });

            if (_settings["Toggle"].AsBool())
                Toggle = true;

            Chat.WriteLine("AssistManager Loaded! Version: 1.0.0.0");
            Chat.WriteLine("/assistmanager for settings.");

            AssistPlayer = Config.CharSettings[Game.ClientInst].AssistPlayer;
            Tick = Config.CharSettings[Game.ClientInst].Tick;
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void AssistPlayer_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].AssistPlayer = e;
            AssistPlayer = e;
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\AssistManagerInfoView.xml",
                windowSize: new Rect(0, 0, 250, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F4) && !_init)
            //{
            //    _init = true;

            //    Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\HelpManager\\{Game.ClientInst}\\Config.json");

            //    SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 250, 300), "Help Manager", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

            //    if (SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsVisible)
            //    {
            //        foreach (string settingsName in SettingsController.settingsWindows.Keys.Where(x => x.Contains("Help Manager")))
            //        {
            //            SettingsController.AppendSettingsTab(settingsName, SettingsController.settingsWindow);

            //            SettingsController.settingsWindow.FindView("AssistNamedCharacter", out TextInputView assistInput);

            //            if (assistInput != null)
            //                assistInput.Text = Config.CharSettings[Game.ClientInst].AssistPlayer;
            //        }
            //    }

            //    _init = false;
            //}

            if (Game.IsZoning)
                return;

            if (Toggle && Time.NormalTime > _timer + Tick)
            {
                if (ModeSelection.OutsideAssist == (ModeSelection)_settings["ModeSelection"].AsInt32())
                {
                    SimpleChar _mob = DynelManager.NPCs
                        .Where(c => c.Health > 0 && c.HealthPercent < 35
                            && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 15f)
                        .FirstOrDefault();

                    if (_mob != null && DynelManager.LocalPlayer.MovementState != MovementState.Sit 
                        && DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                    { DynelManager.LocalPlayer.Attack(_mob, false); }
                }
                else if (ModeSelection.Assist == (ModeSelection)_settings["ModeSelection"].AsInt32() && !string.IsNullOrEmpty(AssistPlayer))
                {
                    SimpleChar _player = DynelManager.Characters
                        .Where(c => !string.IsNullOrEmpty(AssistPlayer)
                            && c.Health > 0 && !c.Flags.HasFlag(CharacterFlags.Pet)
                            && c.Name.ToLower() == AssistPlayer.ToLower())
                        .FirstOrDefault();

                    if (_player != null && DynelManager.LocalPlayer.MovementState != MovementState.Sit)
                    {
                        if (_player?.FightingTarget == null && DynelManager.LocalPlayer.FightingTarget != null)
                        { DynelManager.LocalPlayer.StopAttack(); }

                        if (_player?.FightingTarget != null
                            && ((DynelManager.LocalPlayer.FightingTarget == null && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                || (DynelManager.LocalPlayer.FightingTarget != null && DynelManager.LocalPlayer.FightingTarget.Identity != _player?.FightingTarget.Identity)))
                        { DynelManager.LocalPlayer.Attack(_player.FightingTarget, false); }
                    }
                }

                _timer = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);
                SettingsController.settingsWindow.FindView("AssistNamedCharacter", out TextInputView assistInput);

                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;
                if (assistInput != null && !string.IsNullOrEmpty(assistInput.Text))
                    if (Config.CharSettings[Game.ClientInst].AssistPlayer != assistInput.Text)
                        Config.CharSettings[Game.ClientInst].AssistPlayer = assistInput.Text;

                if (SettingsController.settingsWindow.FindView("AssistManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }

                if (_settings["Toggle"].AsBool() && !Toggle)
                {
                    Toggle = true;
                    Chat.WriteLine($"Assist : {_settings["Toggle"].AsBool()}");
                }

                if (!_settings["Toggle"].AsBool() && Toggle)
                {
                    Toggle = false;
                    Chat.WriteLine($"Assist : {_settings["Toggle"].AsBool()}");
                }
            }

            #endregion
        }

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].AssistPlayerChangedEvent -= AssistPlayer_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].AssistPlayerChangedEvent += AssistPlayer_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        public enum ModeSelection
        {
            None, Assist, OutsideAssist
        }
    }
}
