﻿using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncManager.IPCMessages
{
    [AoContract((int)IPCOpcode.KnuBotTradeFinish)]
    public class KnuBotTradeFinishMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.KnuBotTradeFinish;

        [AoMember(0)]
        public Identity Target { get; set; }

        [AoMember(1)]
        public int Decline { get; set; }

        [AoMember(2)]
        public int Amount { get; set; }
    }
}
