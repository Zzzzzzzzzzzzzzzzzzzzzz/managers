﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace SyncManager.IPCMessages
{
    [AoContract((int)IPCOpcode.SocialAction)]
    public class SocialActionMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.SocialAction;

        [AoMember(0)]
        public SocialAction Action { get; set; }
    }
}
