﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace SyncManager.IPCMessages
{
    [AoContract((int)IPCOpcode.UnEquip)]
    public class UnEquipMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.UnEquip;

        [AoMember(0)]
        public int ItemId { get; set; }

        [AoMember(1)]
        public EquipSlot EquipSlot { get; set; }
    }
}
