﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncManager.IPCMessages
{
    public enum IPCOpcode
    {
        Move = 200,
        Jump = 201,
        Sitting = 202,
        Target = 203,
        Attack = 204,
        StopAttack = 205,
        Use = 206,
        KnuBotChatOpen = 207,
        KnuBotChatClose = 208,
        KnuBotChatAnswer = 209,
        KnuBotTradeStart = 210,
        KnuBotTradeContainer = 211,
        KnuBotTradeFinish = 212,
        UseItem = 213,
        Trade = 214,
        Cast = 215,
        Remove = 216,
        Equip = 217,
        UnEquip = 218,
        SocialAction = 219
    }
}
