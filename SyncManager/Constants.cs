﻿using AOSharp.Common.GameData;
using System.Collections.Generic;

namespace SyncManager
{
    public static class Constants
    {
        public static readonly int[] _noviRings =
        {
            226308,
            226290,
            226291,
            226307,
            226288
        };

        public static readonly int[] _rimyRings =
        {
            226188,
            226189,
            226190,
            226191,
            226192
        };

        public static readonly int[] _achromRings =
        {
            226065,
            226066,
            226067,
            226068,
            226069
        };

        public static readonly int[] _sangRings =
        {
            226287,
            226293,
            226294,
            226295,
            226306
        };

        public static readonly int[] _caligRings =
        {
            226125,
            226127,
            226126,
            226023,
            226005
        };

        public static readonly int[] _exempts =
        {
            305476,
            204698,
            156576,
            267168,
            267167,
            128200
        };

        public static readonly int[] _ignores =
        {
            291043,
            204104,
            204105,
            204106,
            204107,
            303136,
            303137,
            303138,
            303141,
            204698,
            204653,
            206013,
            267168,
            267167,
            305478,
            303179
        };
    }
}
