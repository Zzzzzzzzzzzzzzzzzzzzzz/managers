﻿using System;
using System.Linq;
using System.Diagnostics;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using AOSharp.Core.Inventory;
using AOSharp.Common.GameData.UI;
using SyncManager.IPCMessages;
using System.Threading;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using KnuBotTradeMessage = SmokeLounge.AOtomation.Messaging.Messages.N3Messages.KnuBotTradeMessage;

namespace SyncManager
{
    public class SyncManager : AOPluginEntry
    {
        private static IPCChannel IPCChannel;

        public static Config Config { get; private set; }

        protected Settings _settings;

        private static Identity useDynel;
        private static Identity useOnDynel;
        private static Identity useItem;

        private static List<Item> _cachedInv;

        private static Item _bagItem;

        public static double _openTimer = 0;
        private static bool _initBags = false;

        private static bool _init = false;

        public static bool _subbedUIEvents = false;

        public static float Tick = 0;

        private static double _useTimer;

        public static Window _infoWindow;

        public static string PluginDir;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        private bool IsActiveWindow => GetForegroundWindow() == Process.GetCurrentProcess().MainWindowHandle;

        public override void Run(string pluginDir)
        {
            _settings = new Settings("SyncManager");
            PluginDir = pluginDir;

            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\SyncManager\\{Game.ClientInst}\\Config.json");
            IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

            Game.OnUpdate += OnUpdate;
            Network.N3MessageReceived += Network_N3MessageReceived;
            Network.N3MessageSent += Network_N3MessageSent;

            Game.TeleportEnded += OnZoned;

            _settings.AddVariable("SyncTarget", false);
            _settings.AddVariable("SyncCombat", true);
            _settings.AddVariable("SyncMove", false);
            _settings.AddVariable("SyncSitting", false);
            _settings.AddVariable("OpenBags", true);
            _settings.AddVariable("SyncUse", true);
            _settings.AddVariable("SyncEquip", false);
            _settings.AddVariable("SyncNPCChat", false);
            _settings.AddVariable("SyncNPCTrade", false);
            _settings.AddVariable("SyncYalm", true);
            _settings.AddVariable("SyncNanos", false);
            _settings.AddVariable("SyncEmote", true);

            IPCChannel.RegisterCallback((int)IPCOpcode.Move, OnMoveMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Jump, OnJumpMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Sitting, OnSittingMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Cast, OnCastMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Remove, OnRemoveMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.Equip, OnEquipMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.UnEquip, OnUnEquipMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.SocialAction, OnSocialActionMessage);

            IPCChannel.RegisterCallback((int)IPCOpcode.Attack, OnAttackMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.StopAttack, OnStopAttackMessage);

            //IPCChannel.RegisterCallback((int)IPCOpcode.Trade, OnTradeMessage);

            IPCChannel.RegisterCallback((int)IPCOpcode.Use, OnUseMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.UseItem, OnUseItemMessage);

            IPCChannel.RegisterCallback((int)IPCOpcode.Target, OnTargetMessage);

            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotChatOpen, OnKnuBotChatOpenMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotChatClose, OnKnuBotCloseMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotChatAnswer, OnKnuBotAnswerMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotTradeContainer, OnKnuBotTradeContainerMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotTradeStart, OnKnuBotTradeStartMessage);
            IPCChannel.RegisterCallback((int)IPCOpcode.KnuBotTradeFinish, OnKnuBotTradeFinishMessage);

            RegisterSettingsWindow("Sync Manager", "SyncManagerSettingsView.xml");

            Chat.RegisterCommand("synctarget", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncTarget"] = !_settings["SyncTarget"].AsBool();
                Chat.WriteLine($"Sync target : {_settings["SyncTarget"].AsBool()}");
            });
            Chat.RegisterCommand("synccombat", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncCombat"] = !_settings["SyncCombat"].AsBool();
                Chat.WriteLine($"Sync combat : {_settings["SyncCombat"].AsBool()}");
            });
            Chat.RegisterCommand("syncmove", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncMove"] = !_settings["SyncMove"].AsBool();
                Chat.WriteLine($"Sync move : {_settings["SyncMove"].AsBool()}");
            });
            Chat.RegisterCommand("syncsitting", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncSitting"] = !_settings["SyncSitting"].AsBool();
                Chat.WriteLine($"Sync sitting : {_settings["SyncSitting"].AsBool()}");
            });
            Chat.RegisterCommand("openbags", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["OpenBags"] = !_settings["OpenBags"].AsBool();
                Chat.WriteLine($"Open bags : {_settings["OpenBags"].AsBool()}");
            });
            Chat.RegisterCommand("syncuse", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncUse"] = !_settings["SyncUse"].AsBool();
                Chat.WriteLine($"Sync use : {_settings["SyncUse"].AsBool()}");
            });
            Chat.RegisterCommand("syncequip", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncEquip"] = !_settings["SyncEquip"].AsBool();
                Chat.WriteLine($"Sync equip : {_settings["SyncEquip"].AsBool()}");
            });
            Chat.RegisterCommand("syncnpcchat", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncNPCChat"] = !_settings["SyncNPCChat"].AsBool();
                Chat.WriteLine($"Sync NPC chatting : {_settings["SyncNPCChat"].AsBool()}");
            });
            Chat.RegisterCommand("syncnpctrade", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncNPCTrade"] = !_settings["SyncNPCTrade"].AsBool();
                Chat.WriteLine($"Sync NPC trading : {_settings["SyncNPCTrade"].AsBool()}");
            });
            Chat.RegisterCommand("syncyalm", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncYalm"] = !_settings["SyncYalm"].AsBool();
                Chat.WriteLine($"Sync yalm : {_settings["SyncYalm"].AsBool()}");
            });
            Chat.RegisterCommand("syncemote", (string command, string[] param, ChatWindow chatWindow) =>
            {
                _settings["SyncEmote"] = !_settings["SyncEmote"].AsBool();
                Chat.WriteLine($"Sync emote : {_settings["SyncEmote"].AsBool()}");
            });

            Chat.WriteLine("SyncManager Loaded! Version: 0.9.9.88");
            Chat.WriteLine("/syncmanager for settings.");

            Tick = Config.CharSettings[Game.ClientInst].Tick;
        }

        private void OnUpdate(object s, float deltaTime)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.F2) && !_init)
            //{
            //    _init = true;

            //    Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\SyncManager\\{Game.ClientInst}\\Config.json");

            //    SettingsController.settingsWindow = Window.Create(new Rect(50, 50, 250, 300), "Sync Manager", "Settings", WindowStyle.Default, WindowFlags.AutoScale);

            //    if (SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsVisible)
            //    {
            //        foreach (string settingsName in SettingsController.settingsWindows.Keys.Where(x => x.Contains("Sync Manager")))
            //        {
            //            SettingsController.AppendSettingsTab(settingsName, SettingsController.settingsWindow);

            //            SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

            //            if (channelInput != null)
            //                channelInput.Text = $"{Config.CharSettings[Game.ClientInst].IPCChannel}";
            //        }
            //    }

            //    _init = false;
            //}

            if (Game.IsZoning) { return; }

            if (!_initBags && _settings["OpenBags"].AsBool())
            {
                List<Item> bags = Inventory.Items
                    .Where(c => c.UniqueIdentity.Type == IdentityType.Container)
                    .ToList();

                foreach (Item bag in bags)
                {
                    bag.Use();
                    bag.Use();
                }

                _initBags = true;
            }

            if (Time.NormalTime > _useTimer + Tick)
            {
                if (!IsActiveWindow) { ListenerUseSync(); }

                _useTimer = Time.NormalTime;
            }

            #region UI Update

            if (_subbedUIEvents && SettingsController.settingsWindow != null && !SettingsController.settingsWindow.IsValid)
            { UnSubUIEvents(); }

            if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
            {
                SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                SettingsController.settingsWindow.FindView("TickBox", out TextInputView tickInput);

                if (channelInput != null && !string.IsNullOrEmpty(channelInput.Text))
                    if (int.TryParse(channelInput.Text, out int channelValue)
                        && Config.CharSettings[Game.ClientInst].IPCChannel != channelValue)
                        Config.CharSettings[Game.ClientInst].IPCChannel = channelValue;
                if (tickInput != null && !string.IsNullOrEmpty(tickInput.Text))
                    if (float.TryParse(tickInput.Text, out float tickValue)
                        && Config.CharSettings[Game.ClientInst].Tick != tickValue)
                        Config.CharSettings[Game.ClientInst].Tick = tickValue;

                if (SettingsController.settingsWindow.FindView("SyncManagerInfoView", out Button infoView))
                {
                    infoView.Tag = SettingsController.settingsWindow;
                    infoView.Clicked = HandleInfoViewClick;
                }
            }

            #endregion
        }

        #region Callbacks

        private void OnJumpMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            JumpMessage jumpMsg = (JumpMessage)msg;

            if (Playfield.Identity.Instance == jumpMsg.PlayfieldId)
                MovementController.Instance.SetMovement(jumpMsg.MoveType);
        }

        private void OnSittingMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            SittingMessage sittingMsg = (SittingMessage)msg;

            if (Playfield.Identity.Instance == sittingMsg.PlayfieldId)
                MovementController.Instance.SetMovement(sittingMsg.MoveType);
        }

        private void OnRemoveMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncYalm"].AsBool())
            {
                RemoveMessage remMsg = (RemoveMessage)msg;

                DynelManager.LocalPlayer.Buffs
                    .FirstOrDefault(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"))?
                    .Remove();

                Item _item = Inventory.Items.FirstOrDefault(c => c.Slot.Type == IdentityType.WeaponPage && c.Name.Contains("Yalm"));

                { _item?.MoveToInventory(); }
            }
        }

        private void OnCastMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncYalm"].AsBool())
            {
                CastMessage castMsg = (CastMessage)msg;

                Spell _spell = Spell.List
                        .Where(c => c.UseModifiers
                                    .Where(x => x.Function == SpellFunction.EnableFlight).Count() > 0)
                        .FirstOrDefault(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"));

                if (Spell.Find(castMsg.NanoId, out Spell spell)) 
                {
                    RemoveMorph();
                    spell?.Cast();
                }
                else if (_spell != null)
                {
                    RemoveMorph();
                    _spell.Cast();
                }
                else
                {
                    Item _item = Inventory.Items.FirstOrDefault(c => c.Slot.Type == IdentityType.Inventory && c.Name.Contains("Yalm"));

                    if (_item != null)
                    {
                        RemoveMorph();
                        _item.Equip(_item.EquipSlots.FirstOrDefault());
                    }
                }
            }
            else if (_settings["SyncNanos"].AsBool())
            {
                CastMessage castMsg = (CastMessage)msg;

                Spell _spell = Spell.List.FirstOrDefault(c => c.Id == castMsg.NanoId);

                if (_spell != null)
                {
                    SimpleChar _target = DynelManager.Characters.FirstOrDefault(c => c.Identity == castMsg.Target);

                    if (_target != null) { _spell.Cast(_target, true); }
                }
            }
        }

        private void OnEquipMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncYalm"].AsBool())
            {
                EquipMessage equipMsg = (EquipMessage)msg;

                Item _item = Inventory.Items.FirstOrDefault(c => c.Slot.Type == IdentityType.Inventory && c.Name.Contains("Yalm"));

                Spell _spell = Spell.List
                        .Where(c => c.UseModifiers
                                    .Where(x => x.Function == SpellFunction.EnableFlight).Count() > 0)
                        .FirstOrDefault(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"));

                if (_item != null)
                {
                    RemoveMorph();
                    _item.Equip(_item.EquipSlots.FirstOrDefault());
                }
                else if (_spell != null)
                {
                    RemoveMorph();
                    _spell.Cast();
                }

            }

            if (_settings["SyncEquip"].AsBool())
            {
                EquipMessage equipMsg = (EquipMessage)msg;

                Item _item = Inventory.Items.FirstOrDefault(c => c.Id == equipMsg.ItemId);

                _item?.Equip(equipMsg.EquipSlot);
            }
        }

        private void OnUnEquipMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncYalm"].AsBool())
            {
                UnEquipMessage unEquipMsg = (UnEquipMessage)msg;

                DynelManager.LocalPlayer.Buffs
                    .FirstOrDefault(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"))?
                    .Remove();

                Item _item = Inventory.Items.FirstOrDefault(c => c.Slot.Type == IdentityType.WeaponPage && c.Name.Contains("Yalm"));

                { _item?.MoveToInventory(); }
            }

            if (_settings["SyncEquip"].AsBool())
            {
                UnEquipMessage unEquipMsg = (UnEquipMessage)msg;

                Item _item = Inventory.Items.FirstOrDefault(c => c.Id == unEquipMsg.ItemId && c.Slot.Type != IdentityType.Inventory);

                { _item?.MoveToInventory(); }
            }
        }

        private void OnSocialActionMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            SocialActionMessage socialActionMsg = (SocialActionMessage)msg;

            Network.Send(new SocialActionCmdMessage { Unknown5 = 0x3E, Unknown = 1, Action = socialActionMsg.Action });
        }

        private void OnMoveMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            MoveMessage moveMsg = (MoveMessage)msg;

            if (Playfield.Identity.Instance == moveMsg?.PlayfieldId
                && DynelManager.Players.Any(c => c.Identity.Instance == sender && c.DistanceFrom(DynelManager.LocalPlayer) < 20f))
            {
                DynelManager.LocalPlayer.Position = moveMsg.Position;
                DynelManager.LocalPlayer.Rotation = moveMsg.Rotation;
                MovementController.Instance.SetMovement(moveMsg.MoveType);
            }
        }

        //private void OnTradeMessage(int sender, IPCMessage msg)
        //{
        //    if (Game.IsZoning || IsActiveWindow) { return; }

        //    TradeHandleMessage charTradeIpcMsg = (TradeHandleMessage)msg;

        //    if (charTradeIpcMsg.Action == TradeAction.Accept)
        //    {
        //        Network.Send(new TradeMessage()
        //        {
        //            Unknown1 = 2,
        //            Action = (TradeAction)3,
        //        });
        //    }
        //    else if (charTradeIpcMsg.Action == TradeAction.Confirm)
        //    {
        //        Network.Send(new TradeMessage()
        //        {
        //            Unknown1 = 2,
        //            Action = (TradeAction)1,
        //        });
        //    }
        //}

        private void OnZoned(object s, EventArgs e)
        {
            if (!_init && _settings["OpenBags"].AsBool())
            {
                _init = true;
                Task.Factory.StartNew(
                    async () =>
                    {
                        await Task.Delay(500);
                        foreach (Item bag in Inventory.Items
                            .Where(c => c.UniqueIdentity.Type == IdentityType.Container
                                    && Inventory.Backpacks
                                        .Where(x => !x.IsOpen)
                                        .Select(x => x.Identity)
                                        .Contains(c.UniqueIdentity)))
                        {
                            bag.Use();
                            bag.Use();
                        }
                        _init = false;
                    });
            }
        }

        private void Network_N3MessageReceived(object s, N3Message n3Msg)
        {
            if (!IsActiveCharacter() || n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.KnubotStartTrade) { _cachedInv = Inventory.Items; }
        }

        private void Network_N3MessageSent(object s, N3Message n3Msg)
        {
            if (!IsActiveCharacter() || n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

            if (n3Msg.N3MessageType == N3MessageType.CharDCMove)
            {
                if (_settings["SyncMove"].AsBool())
                {
                    CharDCMoveMessage charDCMoveMsg = (CharDCMoveMessage)n3Msg;

                    if (charDCMoveMsg.MoveType == MovementAction.JumpStart)
                    {
                        IPCChannel.Broadcast(new JumpMessage()
                        {
                            MoveType = charDCMoveMsg.MoveType,
                            PlayfieldId = Playfield.Identity.Instance,
                        });
                    }
                    else
                    {
                        IPCChannel.Broadcast(new MoveMessage()
                        {
                            MoveType = charDCMoveMsg.MoveType,
                            PlayfieldId = Playfield.Identity.Instance,
                            Position = charDCMoveMsg.Position,
                            Rotation = charDCMoveMsg.Heading
                        });
                    }
                }
                else if (_settings["SyncSitting"].AsBool())
                {
                    CharDCMoveMessage charDCMoveMsg = (CharDCMoveMessage)n3Msg;

                    if (charDCMoveMsg.MoveType == MovementAction.SwitchToSit)
                    {
                        IPCChannel.Broadcast(new SittingMessage()
                        {
                            MoveType = charDCMoveMsg.MoveType,
                            PlayfieldId = Playfield.Identity.Instance,
                        });
                    }
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.SocialActionCmd)
            {
                if (_settings["SyncEmote"].AsBool())
                {
                    SocialActionCmdMessage socialActionCmdMsg = (SocialActionCmdMessage)n3Msg;

                    IPCChannel.Broadcast(new SocialActionMessage()
                    {
                        Action = socialActionCmdMsg.Action
                    });
                }
            }
            //else if (n3Msg.N3MessageType == N3MessageType.Trade)
            //{
            //    if (_settings["SyncTrade"].AsBool())
            //    {
            //        TradeMessage tradeMsg = (TradeMessage)n3Msg;

            //        if (tradeMsg.Action == TradeAction.Accept)
            //        {
            //            IPCChannel.Broadcast(new TradeHandleMessage()
            //            {
            //                Unknown1 = 2,
            //                Action = (TradeAction)3,
            //            });
            //        }
            //        else if (tradeMsg.Action == TradeAction.Confirm)
            //        {
            //            IPCChannel.Broadcast(new TradeHandleMessage()
            //            {
            //                Unknown1 = 2,
            //                Action = (TradeAction)1,
            //            });
            //        }

            //    }
            //}
            else if (n3Msg.N3MessageType == N3MessageType.ClientMoveItemToInventory)
            {
                if (_settings["SyncYalm"].AsBool())
                {
                    ClientMoveItemToInventory invItemMsg = (ClientMoveItemToInventory)n3Msg;

                    if (Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer)?.Name.Contains("Yalm") == true)
                        if (invItemMsg.SourceContainer == IdentityType.Inventory) 
                        {
                            IPCChannel.Broadcast(new EquipMessage()
                            {
                                ItemId = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).Id,
                                EquipSlot = EquipSlot.Weap_Hud1
                            });
                        }
                        else if (invItemMsg.SourceContainer == IdentityType.WeaponPage) 
                        { 
                            IPCChannel.Broadcast(new UnEquipMessage()
                            {
                                ItemId = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).Id,
                                EquipSlot = EquipSlot.Weap_Hud1
                            });
                        }
                }
                if (_settings["SyncEquip"].AsBool())
                {
                    ClientMoveItemToInventory invItemMsg = (ClientMoveItemToInventory)n3Msg;

                    if (invItemMsg.SourceContainer == IdentityType.Inventory)
                    {
                        IPCChannel.Broadcast(new EquipMessage()
                        {
                            ItemId = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).Id,
                            EquipSlot = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).EquipSlots.FirstOrDefault()
                        });
                    }
                    else if (invItemMsg.SourceContainer != IdentityType.Inventory)
                    {
                        IPCChannel.Broadcast(new UnEquipMessage()
                        {
                            ItemId = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).Id,
                            EquipSlot = Inventory.Items.FirstOrDefault(c => c.Slot == invItemMsg.SourceContainer).EquipSlots.FirstOrDefault()
                        });
                    }
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
            {
                CharacterActionMessage charActionMsg = (CharacterActionMessage)n3Msg;

                if (charActionMsg.Action == CharacterActionType.StandUp)
                {
                    if (_settings["SyncMove"].AsBool())
                    {
                        IPCChannel.Broadcast(new MoveMessage()
                        {
                            MoveType = MovementAction.LeaveSit,
                            PlayfieldId = Playfield.Identity.Instance,
                            Position = DynelManager.LocalPlayer.Position,
                            Rotation = DynelManager.LocalPlayer.Rotation
                        });
                    }
                    else if (_settings["SyncSitting"].AsBool())
                    {
                        IPCChannel.Broadcast(new SittingMessage()
                        {
                            MoveType = MovementAction.LeaveSit,
                            PlayfieldId = Playfield.Identity.Instance,
                        });
                    }
                }
                else if (charActionMsg.Action == CharacterActionType.CastNano)
                {
                    if (_settings["SyncYalm"].AsBool())
                    {
                        if (Spell.Find(charActionMsg.Parameter2, out Spell spell)
                            && (spell.Name.Contains("Yalm") || spell.Name.Contains("Phasefront") || spell.Name.Contains("Kodaik")))
                        {
                            IPCChannel.Broadcast(new CastMessage()
                            {
                                NanoId = charActionMsg.Parameter2,
                            });
                        }
                    }
                    else if (_settings["SyncNanos"].AsBool())
                    {
                        if (Spell.Find(charActionMsg.Parameter2, out Spell spell))
                        {
                            IPCChannel.Broadcast(new CastMessage()
                            {
                                NanoId = charActionMsg.Parameter2,
                                Target = charActionMsg.Target
                            });
                        }
                    }
                }
                else if (charActionMsg.Action == CharacterActionType.RemoveFriendlyNano)
                {
                    if (_settings["SyncYalm"].AsBool())
                    {
                        if (Spell.Find(charActionMsg.Parameter2, out Spell spell)
                            && (spell.Name.Contains("Yalm") || spell.Name.Contains("Phasefront") || spell.Name.Contains("Kodaik")))
                            IPCChannel.Broadcast(new RemoveMessage()
                            {
                                NanoId = charActionMsg.Parameter2
                            });
                    }
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.LookAt)
            {
                if (_settings["SyncTarget"].AsBool())
                {
                    LookAtMessage lookAtMsg = (LookAtMessage)n3Msg;

                    Identity _identity = DynelManager.AllDynels.FirstOrDefault(c => c.Identity == lookAtMsg.Target).Identity;

                    IPCChannel.Broadcast(new TargetMessage()
                    {
                        Target = _identity
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.Attack)
            {
                if (_settings["SyncCombat"].AsBool())
                {
                    AttackMessage attackMsg = (AttackMessage)n3Msg;

                    IPCChannel.Broadcast(new AttackIPCMessage()
                    {
                        Target = attackMsg.Target
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.StopFight)
            {
                if (_settings["SyncCombat"].AsBool())
                {
                    StopFightMessage stopAttackMsg = (StopFightMessage)n3Msg;

                    IPCChannel.Broadcast(new StopAttackIPCMessage());
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.GenericCmd)
            {
                if (_settings["SyncUse"].AsBool())
                {
                    GenericCmdMessage genericCmdMsg = (GenericCmdMessage)n3Msg;

                    if (genericCmdMsg.Action == GenericCmdAction.Use)
                    {
                        if (genericCmdMsg.Target.Type == IdentityType.Terminal)
                        {
                            IPCChannel.Broadcast(new UseMessage()
                            {
                                Target = genericCmdMsg.Target,
                                PfId = Playfield.ModelIdentity.Instance
                            });
                        }
                        else if (Inventory.Find(genericCmdMsg.Target, out Item item) && item.UniqueIdentity == Identity.None
                            && !Constants._exempts.Contains(item.Id) && !item.Name.Contains("Health"))
                        {
                            IPCChannel.Broadcast(new UsableMessage()
                            {
                                ItemLowId = item.Id,
                                ItemHighId = item.HighId,
                            });
                        }
                        else
                        {
                            foreach (Backpack bag in Inventory.Backpacks)
                            {
                                _bagItem = bag.Items
                                    .Where(c => c.Slot.Instance == genericCmdMsg.Target.Instance)
                                    .FirstOrDefault();

                                if (_bagItem != null)
                                {
                                    IPCChannel.Broadcast(new UsableMessage()
                                    {
                                        ItemLowId = _bagItem.Id,
                                        ItemHighId = _bagItem.HighId
                                    });
                                }
                            }
                        }
                    }
                    else if (genericCmdMsg.Action == GenericCmdAction.UseItemOnItem)
                    {
                        if (Inventory.Find((Identity)genericCmdMsg.Source, out Item item))
                        {
                            IPCChannel.Broadcast(new UsableMessage()
                            {
                                ItemLowId = item.Id,
                                ItemHighId = item.HighId,
                                Target = genericCmdMsg.Target
                            });
                        }
                        else
                        {
                            foreach (Backpack bag in Inventory.Backpacks)
                            {
                                _bagItem = bag.Items
                                    .Where(c => c.Slot.Instance == genericCmdMsg.Source.Value.Instance)
                                    .FirstOrDefault();

                                if (_bagItem != null)
                                {
                                    IPCChannel.Broadcast(new UsableMessage()
                                    {
                                        ItemLowId = _bagItem.Id,
                                        ItemHighId = _bagItem.HighId,
                                        Target = genericCmdMsg.Target
                                    });
                                }
                            }
                        }
                    }
                }
            }
            //else if (n3Msg.N3MessageType == N3MessageType.Trade)
            //{
            //    if (!_settings["SyncTrade"].AsBool()) { return; }

            //    TradeMessage charTradeIpcMsg = (TradeMessage)n3Msg;

            //    if (charTradeIpcMsg.Action == TradeAction.Confirm)
            //    {
            //        IPCChannel.Broadcast(new TradeHandleMessage()
            //        {
            //            Unknown1 = charTradeIpcMsg.Unknown1,
            //            Action = charTradeIpcMsg.Action,
            //            Target = charTradeIpcMsg.Target,
            //            Container = charTradeIpcMsg.Container,
            //        });
            //    }

            //    if (charTradeIpcMsg.Action == TradeAction.Accept)
            //    {
            //        IPCChannel.Broadcast(new TradeHandleMessage()
            //        {
            //            Unknown1 = charTradeIpcMsg.Unknown1,
            //            Action = charTradeIpcMsg.Action,
            //            Target = charTradeIpcMsg.Target,
            //            Container = charTradeIpcMsg.Container,
            //        });
            //    }
            //}
            else if (n3Msg.N3MessageType == N3MessageType.KnubotOpenChatWindow)
            {
                if (_settings["SyncNPCChat"].AsBool()) 
                {
                    KnuBotOpenChatWindowMessage n3OpenChatMessage = (KnuBotOpenChatWindowMessage)n3Msg;

                    IPCChannel.Broadcast(new KnuBotChatOpenMessage()
                    {
                        Target = n3OpenChatMessage.Target
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.KnubotCloseChatWindow)
            {
                if (_settings["SyncNPCChat"].AsBool()) 
                {
                    KnuBotCloseChatWindowMessage n3CloseChatMessage = (KnuBotCloseChatWindowMessage)n3Msg;

                    IPCChannel.Broadcast(new KnuBotChatCloseMessage()
                    {
                        Target = n3CloseChatMessage.Target
                    });
                }  
            }
            else if (n3Msg.N3MessageType == N3MessageType.KnubotAnswer)
            {
                if (_settings["SyncNPCChat"].AsBool()) 
                {
                    KnuBotAnswerMessage n3AnswerMsg = (KnuBotAnswerMessage)n3Msg;

                    IPCChannel.Broadcast(new KnuBotChatAnswerMessage()
                    {
                        Target = n3AnswerMsg.Target,
                        Answer = n3AnswerMsg.Answer
                    });
                } 
            }
            else if (n3Msg.N3MessageType == N3MessageType.KnubotFinishTrade)
            {
                if (_settings["SyncNPCTrade"].AsBool())
                {
                    KnuBotFinishTradeMessage knuBotFinishTradeMsg = (KnuBotFinishTradeMessage)n3Msg;

                    IPCChannel.Broadcast(new KnuBotTradeFinishMessage
                    {
                        Target = knuBotFinishTradeMsg.Target,
                        Decline = knuBotFinishTradeMsg.Decline,
                        Amount = knuBotFinishTradeMsg.Amount,
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.KnubotStartTrade)
            {
                if (_settings["SyncNPCTrade"].AsBool())
                {
                    KnuBotStartTradeMessage knuBotStartTradeMsg = (KnuBotStartTradeMessage)n3Msg;

                    _cachedInv = Inventory.Items;

                    IPCChannel.Broadcast(new KnuBotTradeStartMessage
                    {
                        Target = knuBotStartTradeMsg.Target
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.KnubotTrade)
            {
                if (_settings["SyncNPCTrade"].AsBool())
                {
                    KnuBotTradeMessage knuBotTradeMsg = (KnuBotTradeMessage)n3Msg;

                    int _cachedItemId = (int)_cachedInv.FirstOrDefault(c => c.Slot.Type == IdentityType.Inventory && c.Slot == knuBotTradeMsg.Container)?.Id;

                    IPCChannel.Broadcast(new KnuBotTradeContainerMessage
                    {
                        Target = knuBotTradeMsg.Target,
                        ItemId = _cachedItemId,
                    });
                }
            }
        }

        private void OnAttackMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncCombat"].AsBool())
            {
                AttackIPCMessage attackMsg = (AttackIPCMessage)msg;

                DynelManager.LocalPlayer.Attack(DynelManager.GetDynel(attackMsg.Target), true);
            }
        }

        private void OnStopAttackMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncCombat"].AsBool())
            {
                DynelManager.LocalPlayer.StopAttack();
            }
        }
        private void OnTargetMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            TargetMessage targetMessage = (TargetMessage)msg;

            Identity _identity = DynelManager.AllDynels.FirstOrDefault(c => c.Identity == targetMessage.Target).Identity;

            Targeting.SetTarget(_identity);
        }

        private void OnUseItemMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            UsableMessage usableMsg = (UsableMessage)msg;

            if (Constants._ignores.Contains(usableMsg.ItemLowId) || Constants._ignores.Contains(usableMsg.ItemHighId)) { return; }

            if (!_settings["SyncUse"].AsBool() || !DynelManager.Players.Any(c => c.Identity.Instance == sender)) { return; }

            if (Constants._noviRings.Contains(usableMsg.ItemLowId))
            {
                Item _noviRings = Inventory.Items
                    .Where(c => c.Name.Contains("Pure Novictum Ring"))
                    .FirstOrDefault();

                if (_noviRings != null)
                {
                    useItem = new Identity(IdentityType.Inventory, _noviRings.Slot.Instance);
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
                else
                {
                    useItem = Inventory.Items
                                .FirstOrDefault(c => c.Name == Inventory.Backpacks
                                                            .FirstOrDefault(x => x.Items
                                                                                .Select(d => d.Name)
                                                                                .Contains("Pure Novictum Ring"))
                                                            .Items.Select(v => v.Name).FirstOrDefault(e => e == "Pure Novictum Ring")).Slot;
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
            }
            else if (Constants._rimyRings.Contains(usableMsg.ItemLowId))
            {
                Item _rimyRings = Inventory.Items
                    .Where(c => c.Name.Contains("Rimy Ring for"))
                    .FirstOrDefault();

                if (_rimyRings != null)
                {
                    useItem = new Identity(IdentityType.Inventory, _rimyRings.Slot.Instance);
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
                else
                {
                    useItem = Inventory.Items
                                .FirstOrDefault(c => c.Name == Inventory.Backpacks
                                                            .FirstOrDefault(x => x.Items
                                                                                .Select(d => d.Name)
                                                                                .Contains("Rimy Ring for"))
                                                            .Items.Select(v => v.Name).FirstOrDefault(e => e == "Rimy Ring for")).Slot;
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
            }
            else if (Constants._achromRings.Contains(usableMsg.ItemLowId))
            {
                Item _achromRings = Inventory.Items
                    .Where(c => c.Name.Contains("Achromic Ring for"))
                    .FirstOrDefault();

                if (_achromRings != null)
                {
                    useItem = new Identity(IdentityType.Inventory, _achromRings.Slot.Instance);
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
                else
                {
                    useItem = Inventory.Items
                                .FirstOrDefault(c => c.Name == Inventory.Backpacks
                                                            .FirstOrDefault(x => x.Items
                                                                                .Select(d => d.Name)
                                                                                .Contains("Achromic Ring for"))
                                                            .Items.Select(v => v.Name).FirstOrDefault(e => e == "Achromic Ring for")).Slot;
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
            }
            else if (Constants._sangRings.Contains(usableMsg.ItemLowId))
            {
                Item _sangRings = Inventory.Items
                    .Where(c => c.Name.Contains("Sanguine Ring for"))
                    .FirstOrDefault();

                if (_sangRings != null)
                {
                    useItem = new Identity(IdentityType.Inventory, _sangRings.Slot.Instance);
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
                else
                {
                    useItem = Inventory.Items
                                .FirstOrDefault(c => c.Name == Inventory.Backpacks
                                                            .FirstOrDefault(x => x.Items
                                                                                .Select(d => d.Name)
                                                                                .Contains("Sanguine Ring for"))
                                                            .Items.Select(v => v.Name).FirstOrDefault(e => e == "Sanguine Ring for")).Slot;
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
            }
            else if (Constants._caligRings.Contains(usableMsg.ItemLowId))
            {
                Item _caligRings = Inventory.Items
                    .Where(c => c.Name.Contains("Caliginous Ring"))
                    .FirstOrDefault();

                if (_caligRings != null)
                {
                    useItem = new Identity(IdentityType.Inventory, _caligRings.Slot.Instance);
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
                else
                {
                    useItem = Inventory.Items
                                .FirstOrDefault(c => c.Name == Inventory.Backpacks
                                                            .FirstOrDefault(x => x.Items
                                                                                .Select(d => d.Name)
                                                                                .Contains("Caliginous Ring"))
                                                            .Items.Select(v => v.Name).FirstOrDefault(e => e == "Caliginous Ring")).Slot;
                    useOnDynel = usableMsg.Target;
                    usableMsg.Target = Identity.None;
                }
            }
            else
            {
                if (usableMsg.Target == Identity.None)
                {
                    if (Inventory.Find(usableMsg.ItemLowId, usableMsg.ItemHighId, out Item item))
                    {
                        Network.Send(new GenericCmdMessage()
                        {
                            Unknown = 1,
                            Action = GenericCmdAction.Use,
                            User = DynelManager.LocalPlayer.Identity,
                            Target = new Identity(IdentityType.Inventory, item.Slot.Instance)
                        });
                    }
                    else
                    {
                        foreach (Backpack bag in Inventory.Backpacks)
                        {
                            _bagItem = bag.Items
                                .Where(c => c.HighId == usableMsg.ItemHighId)
                                .FirstOrDefault();

                            if (_bagItem != null)
                            {
                                Network.Send(new GenericCmdMessage()
                                {
                                    Unknown = 1,
                                    Action = GenericCmdAction.Use,
                                    User = DynelManager.LocalPlayer.Identity,
                                    Target = _bagItem.Slot
                                });
                            }
                        }
                    }
                }
                else
                {
                    if (Inventory.Find(usableMsg.ItemLowId, usableMsg.ItemHighId, out Item item))
                    {
                        useItem = new Identity(IdentityType.Inventory, item.Slot.Instance);
                        useOnDynel = usableMsg.Target;
                        usableMsg.Target = Identity.None;
                    }
                    else
                    {
                        foreach (Backpack bag in Inventory.Backpacks)
                        {
                            _bagItem = bag.Items
                                .Where(c => c.HighId == usableMsg.ItemHighId)
                                .FirstOrDefault();

                            if (_bagItem != null)
                            {
                                Network.Send(new GenericCmdMessage()
                                {
                                    Unknown = 1,
                                    Action = GenericCmdAction.UseItemOnItem,
                                    User = DynelManager.LocalPlayer.Identity,
                                    Target = usableMsg.Target,
                                    Source = _bagItem.Slot
                                });
                            }
                        }
                    }
                }
            }
        }

        private void OnUseMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            if (_settings["SyncUse"].AsBool())
            {
                UseMessage useMsg = (UseMessage)msg;

                if (useMsg.PfId == Playfield.ModelIdentity.Instance)
                {
                    useDynel = useMsg.Target;
                }
            }
        }

        private void OnKnuBotChatOpenMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotChatOpenMessage knuBotChatOpenMsg = (KnuBotChatOpenMessage)msg;

            NpcDialog.Open(knuBotChatOpenMsg.Target);
        }

        private void OnKnuBotCloseMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotChatCloseMessage knuBotChatCloseMsg = (KnuBotChatCloseMessage)msg;

            KnuBotCloseChatWindowMessage knuBotChatCloseWindowMessage = new KnuBotCloseChatWindowMessage()
            {
                Unknown1 = 2,
                Unknown2 = 0,
                Unknown3 = 0,
                Target = knuBotChatCloseMsg.Target
            };

            Network.Send(knuBotChatCloseWindowMessage);
        }

        private void OnKnuBotAnswerMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotChatAnswerMessage knuBotChatAnswerMsg = (KnuBotChatAnswerMessage)msg;

            NpcDialog.SelectAnswer(knuBotChatAnswerMsg.Target, knuBotChatAnswerMsg.Answer);
        }

        private void OnKnuBotTradeFinishMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotTradeFinishMessage knuBotTradeFinishMsg = (KnuBotTradeFinishMessage)msg;

            Network.Send(new KnuBotFinishTradeMessage()
            {
                Unknown = 0,
                Unknown1 = 2,
                Target = knuBotTradeFinishMsg.Target,
                Decline = knuBotTradeFinishMsg.Decline,
                Amount = knuBotTradeFinishMsg.Amount
            });
        }

        private void OnKnuBotTradeStartMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotTradeStartMessage knuBotTradeStartMsg = (KnuBotTradeStartMessage)msg;

            Network.Send(new KnuBotStartTradeMessage()
            {
                Target = knuBotTradeStartMsg.Target,
                NumberOfItemSlotsInTradeWindow = 0,
                Unknown = 0,
                Unknown1 = 2,
                Message = ""
            });
        }

        private void OnKnuBotTradeContainerMessage(int sender, IPCMessage msg)
        {
            if (Game.IsZoning || IsActiveWindow) { return; }

            KnuBotTradeContainerMessage knuBotTradeContainerMsg = (KnuBotTradeContainerMessage)msg;

            Item _item = Inventory.Items.FirstOrDefault(c => c.Slot.Type == IdentityType.Inventory && c.Id == knuBotTradeContainerMsg.ItemId);

            Network.Send(new KnuBotTradeMessage()
            {
                Identity = DynelManager.LocalPlayer.Identity,
                Target = knuBotTradeContainerMsg.Target,
                Container = _item.Slot,
                Unknown = 0,
                Unknown1 = 2,
                Unknown2 = 0,
                Unknown3 = 0,
                Unknown4 = 0,
            });
        }

        #endregion

        #region Settings

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\SyncManagerInfoView.xml",
                windowSize: new Rect(0, 0, 310, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        #endregion

        #region Misc

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        private void RemoveMorph()
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Polymorph)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AdventurerMorphBuff)
                || DynelManager.LocalPlayer.Buffs.Contains(31593))
            {
                foreach (Buff buff in DynelManager.LocalPlayer.Buffs)
                {
                    if (buff.Id == 31593 || buff.Nanoline == NanoLine.Polymorph || buff.Nanoline == NanoLine.AdventurerMorphBuff)
                        buff.Remove();
                }
            }
        }

        private void ListenerUseSync()
        {
            Vector3 playerPos = DynelManager.LocalPlayer.Position;

            if (useDynel != Identity.None)
            {
                Dynel usedynel = DynelManager.AllDynels.FirstOrDefault(x => x.Identity == useDynel);

                if (usedynel != null && Vector3.Distance(playerPos, usedynel.Position) < 8 &&
                    usedynel.Name != "Rubi-Ka Banking Service Terminal" && usedynel.Name != "Mail Terminal")
                {
                    DynelManager.GetDynel<SimpleItem>(useDynel)?.Use();
                    useDynel = Identity.None;
                }
            }

            if (useOnDynel != Identity.None)
            {
                Dynel _useOnDynel = DynelManager.AllDynels.FirstOrDefault(x => x.Identity == useOnDynel);

                if (_useOnDynel != null && Vector3.Distance(playerPos, _useOnDynel.Position) < 8)
                {
                    Network.Send(new GenericCmdMessage()
                    {
                        Unknown = 1,
                        Action = GenericCmdAction.UseItemOnItem,
                        Temp1 = 0,
                        Temp4 = 0,
                        Identity = DynelManager.LocalPlayer.Identity,
                        User = DynelManager.LocalPlayer.Identity,
                        Target = useOnDynel,
                        Source = useItem

                    });
                    useOnDynel = Identity.None;
                    useItem = Identity.None;
                }
            }
        }

        private bool IsActiveCharacter()
        {
            return IsActiveWindow;
        }

        #endregion
    }
}
